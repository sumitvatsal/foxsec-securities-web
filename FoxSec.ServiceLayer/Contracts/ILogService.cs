﻿using System;

namespace FoxSec.ServiceLayer.Contracts
{
	public interface ILogService
	{
		int CreateLog(int userId, string building,string flag, string node, int? companyId, string action, int? logTypeId=null);
	}
}