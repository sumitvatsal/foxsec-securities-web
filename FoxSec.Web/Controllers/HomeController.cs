﻿using System.Web.Mvc;
using FoxSec.Authentication;
using FoxSec.Infrastructure.EntLib.Logging;
using FoxSec.Web.ViewModels;
using ViewResources;

namespace FoxSec.Web.Controllers
{
    public class HomeController : AuthorizeControllerBase
    {
        public HomeController(ICurrentUser currentUser, ILogger logger) : base(currentUser, logger) {}

        public ActionResult Index()
        {
            ViewData["Message"] = SharedStrings.WelcomeScreenMessage;

            var hmv = CreateViewModel<HomeViewModel>();
            //    if (Session["Visibletab"] == null)
            //    {
            //        return RedirectToAction("LogOn", "Account");
            //    }
            //else  if (Session["Visibletab"].ToString()!="True")
            //    {
            //         return RedirectToAction("LogOn", "Account");
            //    }
            //    //return RedirectToAction("UserLogOn", "Account");
            //    //return RedirectToAction("TabContent","User");

            
            if (Session["Role_ID"] == null)
            {
                return RedirectToAction("LogOn", "Account");
            }
            return View(hmv);
        }

        public ActionResult About()
        {
            var hmv = CreateViewModel<HomeViewModel>();
            return View(hmv);
        }

		[HttpPost]
		public ActionResult SetOpenedTab(string tabIndex)
		{
			HttpContext.Session["tabIndex"] = tabIndex;
			return new EmptyResult();
		}

		[HttpPost]
		public ActionResult SetOpenedSubTab(string tabIndex)
		{
			HttpContext.Session["subTabIndex"] = tabIndex;
			return new EmptyResult();
		}
    }
}