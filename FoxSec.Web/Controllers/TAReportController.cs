﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using FoxSec.Common.Enums;
using FoxSec.Authentication;
using System.Globalization;
using FoxSec.Infrastructure.EF.Repositories;
using FoxSec.Infrastructure.EntLib.Logging;
using FoxSec.ServiceLayer.Contracts;
using FoxSec.DomainModel.DomainObjects;
using FoxSec.Web.Helpers;
using FoxSec.Web.ViewModels;
using System.Web.Mvc;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.SessionState;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.PivotGrid;
using DevExpress.XtraPivotGrid;
using FoxSec.Core.Infrastructure.UnitOfWork;
using System.Data.Entity;
using System.Collections;
using DevExpress.Spreadsheet;
using ClosedXML.Excel;
using System.Data;
using System.IO;
using DevExpress.Printing.ExportHelpers;
using DevExpress.Export;
using DevExpress.Web.Internal;
using DevExpress.Data;
using System.Dynamic;
using DocumentFormat.OpenXml.Wordprocessing;
using DevExpress.Web.DemoUtils;
using DevExpress.Utils;
using System.Web.UI.WebControls;

namespace FoxSec.Web.Controllers
{
    public class TAReportController : BusinessCaseController
    {

        FoxSecDBContext db = new FoxSecDBContext();

        string depname;
        string cname;
        string countryflag = "0";
        #region Install
        private readonly ILogRepository _logRepository;
        private readonly ITAReportRepository _reportRepository;
        private readonly ITAReportService _reportService;
        private readonly IBuildingObjectService _buildingObjectService;
        private readonly ICurrentUser _currentUser;
        private readonly ILogger _logger;
        private readonly IBuildingObjectRepository _BuildingObjectRepository;
        private readonly ITAMoveRepository _TAMoveRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUserDepartmentRepository _UserDepartmentRepository;
        private readonly ITAMoveService _TAMoveService;
        private readonly ITAReportService _TAReportService;


        public TAReportController(ITAReportService reportService,
                                  IUserDepartmentRepository UserDepartmentRepository,
                                  IUserRepository userRepository,
                                  ITAMoveService TAMoveService,
                                  ITAReportService TAReportService,
                                  ILogRepository logRepository,
                                  IBuildingObjectService buildingObjectService,
                                  ITAReportRepository reportRepository,
                                  ITAMoveRepository TAMoveRepository,
                                  IBuildingObjectRepository buildingObjectepository,
                                  ICurrentUser currentUser,
                                  ILogger logger)
            : base(currentUser, logger)
        {
            _logRepository = logRepository;
            _UserDepartmentRepository = UserDepartmentRepository;
            _userRepository = userRepository;
            _TAMoveService = TAMoveService;
            _TAReportService = TAReportService;
            _TAMoveRepository = TAMoveRepository;
            _reportRepository = reportRepository;
            _buildingObjectService = buildingObjectService;
            _reportService = reportService;
            _currentUser = currentUser;
            _logger = logger;
            _BuildingObjectRepository = buildingObjectepository;
        }

        public ActionResult TabContent()
        {

            var hmv = CreateViewModel<HomeViewModel>();
            return PartialView(hmv);

        }
        //public JsonResult SaveBuildingnames()
        #endregion

        [ValidateInput(false)]
        public void ExportToParamsCustomUser(List<int> a)
        {
            Session["Users"] = a;
            //return View();
        }


        void options_CustomizeSheetHeader(DevExpress.Export.ContextEventArgs e)
        {
            try
            {
                // Create a new row.

                int Usrid = Convert.ToInt32(Session["User_Id"]);
                int usrrole = Convert.ToInt32(Session["Role_ID"]);
                string ufname = "";
                string Ulname = "";
                string Userdesignation1 = "";
                int Bul_id = Convert.ToInt32(Session["Buidlidngid"]);
                int companyId1 = Convert.ToInt32(Session["company"]);
                try
                {
                    string getvalidcompid = @" select  FirstName,LastName from Users    where Id={0}";
                    var userLogDeatil = db.Database.SqlQuery<specificUser>(getvalidcompid, Usrid).FirstOrDefault();

                    if (userLogDeatil != null)
                    {
                        ufname = userLogDeatil.FirstName;
                        Ulname = userLogDeatil.LastName;

                    }
                    var userDesignation = db.UserRoles.Where(x => x.Id == usrrole).SingleOrDefault();
                    if (userDesignation != null)
                    {
                        Userdesignation1 = userDesignation.Name;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                string ReportPrepareby = ufname + " " + Ulname + "  " + "(" + Userdesignation1 + ")";

                if (Bul_id == 0 && companyId1 == 0)
                {
                    CellObject row1 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row1.Value = "Report Created:" + ReportPrepareby + " " + "->" + " " + System.DateTime.Now;
                    }
                    else
                    {
                        row1.Value = "Ziņojums ir izveidots:" + ReportPrepareby + " " + "->" + " " + System.DateTime.Now;
                    }


                    try
                    {
                        XlFormattingObject row8Formatting = new XlFormattingObject();
                        row1.Formatting = row8Formatting;
                        row8Formatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Right, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        // row7Formatting.Font = new XlCellFont { Color = System.Drawing.Color.Red, Size = 12 };

                        row8Formatting.BackColor = System.Drawing.Color.Lime;
                        row8Formatting.Font.Bold = true;
                    }
                    catch
                    {
                    }
                    e.ExportContext.AddRow(new[] { row1 });
                    e.ExportContext.AddRow();
                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 0), new DevExpress.Export.Xl.XlCellPosition(5, 0)));
                }

                DateTime From = DateTime.ParseExact(Session["TAStartDate"].ToString(), "dd.MM.yyyy",
                                           System.Globalization.CultureInfo.InvariantCulture);
                DateTime To = DateTime.ParseExact(Session["TAStoptDate"].ToString(), "dd.MM.yyyy",
                                          System.Globalization.CultureInfo.InvariantCulture);

                int companyId = Convert.ToInt32(Session["company"]);

                var CompanyRecords = db.Companies.Where(x => x.Id == companyId).FirstOrDefault();
                string companyName = "", RegistraionNum = "";
                if (CompanyRecords != null)
                {
                    companyName = CompanyRecords.Name;
                    RegistraionNum = CompanyRecords.Comment;
                    if (RegistraionNum == null)
                    {
                        RegistraionNum = "NA";
                    }

                }
                string bname = Session["BuildingName"].ToString();
                var ee = (from Ta in db.TABuildingName
                          join B in db.Buildings on Ta.BuildingId equals B.Id


                          where Ta.Name == bname && Ta.ValidFrom <= From && Ta.ValidTo >= To

                          select new
                          {
                              taname = Ta.Name,
                              Ta.ValidFrom,
                              Ta.ValidTo,
                              Ta.BuildingLicense,
                              Ta.CadastralNr,
                              Ta.Address,
                              B.Name,

                          }).FirstOrDefault();


                if (Bul_id > 0 && companyId1 == 0)
                {
                    // Specify row values.


                    // Specify row values.
                    CellObject row0 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row0.Value = "Report Created:" + ReportPrepareby + " " + "->" + " " + System.DateTime.Now;
                    }
                    else
                    {

                        row0.Value = "Ziņojums ir izveidots:" + ReportPrepareby + " " + "->" + " " + System.DateTime.Now;
                    }

                    // Specify row formatting.
                    try
                    {
                        XlFormattingObject rowFormatting0 = new XlFormattingObject();
                        row0.Formatting = rowFormatting0;
                        rowFormatting0.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Right, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        rowFormatting0.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };

                        rowFormatting0.Font.Bold = true;
                    }
                    catch
                    {

                    }
                    row0.Value = "Report Created:" + ReportPrepareby + " " + "->" + " " + System.DateTime.Now;
                    //row0.Value=row0.Value + ReportPrepareby + " " + "->" + " " + System.DateTime.Now;
                    CellObject row = new CellObject();
                    if (countryflag == "EN")
                    {
                        if (ee.taname == null)
                        {
                            row.Value = "Building Name:" + "";

                        }
                        else
                        {
                            row.Value = "Building Name:" + ee.taname;

                        }

                    }
                    else
                    {
                        row.Value = "Objekta nosaukums:" + ee.taname;
                    }
                    // Specify row formatting.
                    try
                    {
                        XlFormattingObject rowFormatting = new XlFormattingObject();
                        row.Formatting = rowFormatting;
                        rowFormatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        rowFormatting.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };
                        rowFormatting.Font.Bold = true;
                    }
                    catch
                    { }

                    // Add the created row to the output document.

                    CellObject row2 = new CellObject();

                    // Specify row values.
                    if (countryflag == "EN")
                    {
                        row2.Value = "Address:" + ee.Address;
                    }
                    else
                    {
                        row2.Value = "Adrese:" + ee.Address;
                    }
                    try
                    {
                        XlFormattingObject row2Formatting = new XlFormattingObject();
                        row2.Formatting = row2Formatting;
                        row2Formatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        row2Formatting.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };
                        row2Formatting.Font.Bold = true;
                    }
                    catch
                    {
                    }

                    CellObject row3 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row3.Value = "Building License:" + ee.BuildingLicense;
                    }
                    else
                    {
                        row3.Value = "Būvatļauja:" + ee.BuildingLicense;
                    }
                    try
                    {
                        XlFormattingObject row3Formatting = new XlFormattingObject();
                        row3.Formatting = row3Formatting;
                        row3Formatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        row3Formatting.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };
                        row3Formatting.Font.Bold = true;
                    }
                    catch
                    {

                    }

                    CellObject row4 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row4.Value = "Cadastre number:" + ee.CadastralNr;
                    }
                    else
                    {
                        row4.Value = "Kadastra numurs:" + ee.CadastralNr;
                    }
                    try
                    {
                        XlFormattingObject row4Formatting = new XlFormattingObject();
                        row4.Formatting = row4Formatting;
                        row4Formatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        row4Formatting.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };
                        row4Formatting.Font.Bold = true;
                    }
                    catch
                    {

                    }

                    CellObject row5 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row5.Value = "Period:" + From.ToString("dd MMM yyyy") + "--" + To.ToString("dd MMM yyyy");
                    }
                    else
                    {
                        row5.Value = "Atskaites periods: " + From.ToString("dd MMM yyyy") + "--" + To.ToString("dd MMM yyyy");
                    }
                    try
                    {
                        XlFormattingObject row5Formatting = new XlFormattingObject();
                        row5.Formatting = row5Formatting;
                        row5Formatting.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };
                        row5Formatting.Font.Bold = true;
                        row5Formatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Right, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                    }
                    catch
                    {

                    }

                    e.ExportContext.AddRow(new[] { row0 });
                    e.ExportContext.AddRow(new[] { row });
                    e.ExportContext.AddRow(new[] { row2 });
                    e.ExportContext.AddRow(new[] { row3 });
                    e.ExportContext.AddRow(new[] { row4 });
                    e.ExportContext.AddRow(new[] { row5 });

                    // Add an empty row to the output document. Periods:
                    e.ExportContext.AddRow();
                    // Merge cells of two new rows.

                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 0), new DevExpress.Export.Xl.XlCellPosition(5, 0)));

                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 1), new DevExpress.Export.Xl.XlCellPosition(5, 1)));

                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 2), new DevExpress.Export.Xl.XlCellPosition(5, 2)));
                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 3), new DevExpress.Export.Xl.XlCellPosition(5, 3)));
                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 4), new DevExpress.Export.Xl.XlCellPosition(5, 4)));
                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 5), new DevExpress.Export.Xl.XlCellPosition(5, 5)));

                }
                else if (companyId1 > 0 && Bul_id == 0)
                {
                    CellObject row0 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row0.Value = "Report Created:" + ReportPrepareby + " " + "->" + " " + System.DateTime.Now;


                    }
                    else
                    {
                        row0.Value = "Ziņojums ir izveidots:" + ReportPrepareby + " " + "->" + " " + System.DateTime.Now;
                    }
                    try
                    {
                        // Specify row formatting.
                        XlFormattingObject rowFormatting0 = new XlFormattingObject();
                        row0.Formatting = rowFormatting0;
                        //rowFormatting0.BackColor = System.Drawing.Color.LightGray;
                        rowFormatting0.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Right, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        rowFormatting0.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };
                        rowFormatting0.Font.Bold = true;
                    }
                    catch
                    {

                    }
                    //rowFormatting0.BackColor = System.Drawing.Color.Lime;
                    CellObject row01 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row01.Value = "Company Details :" + companyName + "  " + "," + RegistraionNum;
                    }
                    else
                    {
                        row01.Value = "Uzņēmuma informācija :" + companyName + "  " + "," + RegistraionNum;
                    }
                    // Specify row formatting.
                    try
                    {
                        XlFormattingObject row9Formatting = new XlFormattingObject();
                        row01.Formatting = row9Formatting;

                        row9Formatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        // row7Formatting.Font = new XlCellFont { Color = System.Drawing.Color.Red, Size = 12 };
                        row9Formatting.BackColor = System.Drawing.Color.Lime;

                    }
                    catch
                    {

                    }
                    e.ExportContext.AddRow(new[] { row0 });
                    e.ExportContext.AddRow(new[] { row01 });

                    e.ExportContext.AddRow();
                    // Merge cells of two new rows.

                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 0), new DevExpress.Export.Xl.XlCellPosition(5, 0)));

                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 1), new DevExpress.Export.Xl.XlCellPosition(5, 1)));


                }
                else if (Bul_id > 0 && companyId1 > 0)
                {
                    // Specify row values.


                    // Specify row values.
                    CellObject row0 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row0.Value = "Report Created:" + ReportPrepareby + " " + "->" + " " + System.DateTime.Now;

                    }
                    else
                    {
                        row0.Value = "Ziņojums ir izveidots:" + ReportPrepareby + " " + "->" + " " + System.DateTime.Now;

                    }

                    // Specify row formatting.
                    try
                    {
                        XlFormattingObject rowFormatting0 = new XlFormattingObject();
                        row0.Formatting = rowFormatting0;
                        rowFormatting0.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Distributed, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        rowFormatting0.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };
                        rowFormatting0.Font.Bold = true;
                    }
                    catch
                    {

                    }
                    //rowFormatting0.BackColor = System.Drawing.Color.Lime;
                    CellObject row01 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row01.Value = "Company Details :" + companyName + "  " + "," + RegistraionNum;
                    }
                    else
                    {
                        row01.Value = "Uzņēmuma informācija :" + companyName + "  " + "," + RegistraionNum;
                    }
                    try
                    {
                        // Specify row formatting.
                        XlFormattingObject row9Formatting = new XlFormattingObject();
                        row01.Formatting = row9Formatting;
                        row9Formatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        // row7Formatting.Font = new XlCellFont { Color = System.Drawing.Color.Red, Size = 12 };
                        row9Formatting.BackColor = System.Drawing.Color.Lime;

                    }
                    catch
                    {

                    }

                    CellObject row = new CellObject();
                    if (countryflag == "EN")
                    {
                        row.Value = "Building Name:" + ee.taname;

                    }
                    else
                    {
                        row.Value = "objekta nosaukums:" + ee.taname;
                    }
                    try
                    {
                        // Specify row formatting.
                        XlFormattingObject rowFormatting = new XlFormattingObject();
                        row.Formatting = rowFormatting;
                        rowFormatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        rowFormatting.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };

                    }
                    catch
                    {

                    }

                    // Add the created row to the output document.

                    CellObject row2 = new CellObject();

                    // Specify row values.
                    if (countryflag == "EN")
                    {
                        row2.Value = "Address:" + ee.Address;
                    }
                    else
                    {
                        row2.Value = "Adrese:" + ee.Address;
                    }
                    try
                    {
                        XlFormattingObject row2Formatting = new XlFormattingObject();
                        row2.Formatting = row2Formatting;
                        row2Formatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        row2Formatting.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };
                        row2Formatting.Font.Bold = true;

                    }
                    catch
                    {

                    }

                    CellObject row3 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row3.Value = "Building License:" + ee.BuildingLicense;
                    }
                    else
                    {
                        row3.Value = "Būvatļauja:" + ee.BuildingLicense;
                    }
                    try
                    {
                        XlFormattingObject row3Formatting = new XlFormattingObject();
                        row3.Formatting = row3Formatting;
                        row3Formatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        row3Formatting.Font = new XlCellFont { Color = System.Drawing.Color.Red, Size = 12 };
                        row3Formatting.Font.Bold = true;
                    }
                    catch
                    {

                    }

                    CellObject row4 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row4.Value = "Cadastre number:" + ee.CadastralNr;
                    }
                    else
                    {
                        row4.Value = "Kadastra numurs:" + ee.CadastralNr;
                    }
                    try
                    {
                        XlFormattingObject row4Formatting = new XlFormattingObject();
                        row4.Formatting = row4Formatting;
                        row4Formatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        row4Formatting.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };
                        row4Formatting.Font.Bold = true;
                    }
                    catch
                    {

                    }
                    CellObject row5 = new CellObject();
                    if (countryflag == "EN")
                    {
                        row5.Value = "Period:" + ee.ValidFrom.ToString("dd MMM yyyy") + "--" + ee.ValidTo.ToString("dd MMM yyyy");
                    }
                    else
                    {
                        row5.Value = "Atskaites periods:" + ee.ValidFrom.ToString("dd MMM yyyy") + "--" + ee.ValidTo.ToString("dd MMM yyyy");
                    }
                    try
                    {
                        XlFormattingObject row5Formatting = new XlFormattingObject();
                        row5.Formatting = row5Formatting;
                        row5Formatting.Font = new XlCellFont { Color = System.Drawing.Color.Black, Size = 12 };
                        row5Formatting.Alignment = new DevExpress.Export.Xl.XlCellAlignment { HorizontalAlignment = DevExpress.Export.Xl.XlHorizontalAlignment.Center, VerticalAlignment = DevExpress.Export.Xl.XlVerticalAlignment.Top };
                        row5Formatting.Font.Bold = true;
                    }
                    catch
                    {
                    }
                    e.ExportContext.AddRow(new[] { row0 });
                    e.ExportContext.AddRow(new[] { row01 });
                    e.ExportContext.AddRow(new[] { row });
                    e.ExportContext.AddRow(new[] { row2 });
                    e.ExportContext.AddRow(new[] { row3 });
                    e.ExportContext.AddRow(new[] { row4 });
                    e.ExportContext.AddRow(new[] { row5 });
                    // Add an empty row to the output document. Periods:
                    e.ExportContext.AddRow();
                    // Merge cells of two new rows.
                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 0), new DevExpress.Export.Xl.XlCellPosition(5, 0)));
                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 1), new DevExpress.Export.Xl.XlCellPosition(5, 1)));
                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 2), new DevExpress.Export.Xl.XlCellPosition(5, 2)));
                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 3), new DevExpress.Export.Xl.XlCellPosition(5, 3)));
                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 4), new DevExpress.Export.Xl.XlCellPosition(5, 4)));
                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 5), new DevExpress.Export.Xl.XlCellPosition(5, 5)));
                    e.ExportContext.MergeCells(new DevExpress.Export.Xl.XlCellRange(new DevExpress.Export.Xl.XlCellPosition(0, 6), new DevExpress.Export.Xl.XlCellPosition(5, 6)));
                }
            }
            catch
            {

            }
        }

        [HttpPost, ValidateInput(false)]
        public string BatchEditingUpdateBuilding_Name(MVCxGridViewBatchUpdateValues<BuildingNameViewModel, object> updateValues)
        {


            var model = db.TABuildingName;
            foreach (var customer in updateValues.Insert)
            {
                if (updateValues.IsValid(customer))
                {
                    try
                    {
                        var e = new TABuildingNames();
                        e.BuildingId = customer.BuildingId;
                        e.Name = customer.Name;
                        e.ValidFrom = Convert.ToDateTime(customer.ValidFrom);
                        e.ValidTo = Convert.ToDateTime(customer.ValidTo);
                        e.Address = customer.Address;
                        e.BuildingLicense = customer.BuildingLicense;
                        e.CadastralNr = customer.CadastralNr;
                        e.IsDeleted = false;
                        e.Customer = customer.Customer;
                        e.Contractor = customer.Contractor;
                        e.Contract = customer.Contract;
                        e.Sum = customer.Sum;
                        db.TABuildingName.Add(e);
                        // model.Add(TaModel);
                        int k = db.SaveChanges();

                    }
                    catch (Exception e)
                    {
                        updateValues.SetErrorText(customer, e.Message);
                    }
                }
            }
            foreach (var customer in updateValues.Update)
            {
                if (updateValues.IsValid(customer))
                {
                    try
                    {
                        var modelItem = model.FirstOrDefault(it => it.Id == customer.Id);
                        if (modelItem != null)
                        {
                            modelItem.Address = customer.Address;
                            modelItem.Name = customer.Name;
                            modelItem.BuildingId = customer.BuildingId;
                            modelItem.ValidFrom = customer.ValidFrom;
                            modelItem.ValidTo = customer.ValidTo;
                            modelItem.BuildingLicense = customer.BuildingLicense;
                            modelItem.CadastralNr = customer.CadastralNr;
                            modelItem.Customer = customer.Customer;
                            modelItem.Contractor = customer.Contractor;
                            modelItem.Contract = customer.Contract;
                            modelItem.Sum = customer.Sum;
                            this.UpdateModel(modelItem);
                            int k = db.SaveChanges();
                        }
                    }
                    catch (Exception e)
                    {
                        updateValues.SetErrorText(customer, e.Message);
                    }
                }
            }
            foreach (var customer in updateValues.DeleteKeys)
            {
                try
                {
                    int id = Convert.ToInt32(customer);
                    var record = db.TABuildingName.Where(x => x.Id == id).FirstOrDefault();
                    record.IsDeleted = true;
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    updateValues.SetErrorText(customer, e.Message);
                }
            }
            return "";
        }

        #region T&A building

        [HttpGet]
        public ActionResult OpenAllBuildingsObjects(int id)
        {
            int companyid = 0;
            Session["TACompanyId"] = companyid;

            //IEnumerable<BuildingObject> AllBuildings = new List<BuildingObject>();

            var buildings = CreateViewModel<TAGlobalBuildingObjectsViewModel>();

            // Mapper.Map(GetBuildings(), AllBuildings);
            //AllBuildings = GetBuildings();
            //buildings.BuildingObjects = AllBuildings;

            buildings.BuildingObjects = GetBuildingObjects(null);
            //buildings.BuildingObjects.Where(x=>x.GlobalBuilding == 1 );
            return PartialView("TABuildingObjects", buildings);
        }
        public JsonResult GetBuildingsOnClick_FromDate(string datefrom, string dateto)
        {
            DateTime From = DateTime.ParseExact(datefrom, "dd.MM.yyyy",
                                                   System.Globalization.CultureInfo.InvariantCulture);


            DateTime To = DateTime.ParseExact(dateto, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            StringBuilder result = new StringBuilder();
            result.Append("<option value=" + '"' + '"' + ">" + ViewResources.SharedStrings.DefaultDropDownValue + "</option>");
            //var buildings = db.Buildings.ToList();

            // var buildings = db.TABuildingName.Where(x => x.ValidFrom >= From && x.ValidTo <= To).ToList();
            // var buildings= (validfrom >= from && validfrom < to)  or(validto > from && validto < to)
            //var buildings = db.TABuildingName.Where(x => x.ValidFrom >= From && x.ValidFrom < To || x.ValidTo > From && x.ValidTo < To).ToList();
            var buildings = db.TABuildingName.Where(x => x.ValidFrom <= From && x.ValidTo >= From && x.ValidTo <= To || x.ValidTo >= To && x.ValidFrom <= To && x.ValidFrom >= From || x.ValidFrom <= From && x.ValidTo >= To || x.ValidFrom >= From && x.ValidTo <= To).ToList();
            buildings = buildings.Where(x => x.IsDeleted == false).ToList();
            foreach (var cc in buildings)
            {
                result.Append("<option value=" + '"' + cc.BuildingId + '"' + ">" + cc.Name + "</option>");
            }

            return Json(result.ToString(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBuildings(string datefrom, string dateto)
        {
            DateTime From = DateTime.ParseExact(datefrom, "dd.MM.yyyy",
                                        System.Globalization.CultureInfo.InvariantCulture);


            DateTime To = DateTime.ParseExact(dateto, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            StringBuilder result = new StringBuilder();
            result.Append("<option value=" + '"' + '"' + ">" + ViewResources.SharedStrings.DefaultDropDownValue + "</option>");
            //var buildings = db.Buildings.ToList();

            //var buildings = db.TABuildingName.Where(x => x.ValidFrom >= From && x.ValidTo <= To).ToList();
            //var buildings = db.TABuildingName.Where(x => x.ValidFrom >= From && x.ValidFrom < To || x.ValidTo > From && x.ValidTo < To).ToList();
            var buildings = db.TABuildingName.Where(x => x.ValidFrom <= From && x.ValidTo >= From && x.ValidTo <= To || x.ValidTo >= To && x.ValidFrom <= To && x.ValidFrom >= From || x.ValidFrom <= From && x.ValidTo >= To || x.ValidFrom >= From && x.ValidTo <= To).ToList();

            buildings = buildings.Where(x => x.IsDeleted == false).ToList();
            foreach (var cc in buildings)
            {
                result.Append("<option value=" + '"' + cc.BuildingId + '"' + ">" + cc.Name + "</option>");
            }

            return Json(result.ToString(), JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult BuildingsObjectsGridViewPartialA()
        {
            int? CompanyId = (int?)Session["TACompanyId"];
            //return PartialView("TABuildingObjects", GetBuildingObjects(CompanyId));
            return PartialView("TABoGridViewPartialView", GetBuildingObjects(CompanyId));
        }
        [ValidateInput(false)]
        public ActionResult BuildingsObjectsGridViewPartialA1()
        {
            int? CompanyId = (int?)Session["TACompanyId"];
            //return PartialView("TABuildingObjects", GetBuildingObjects(CompanyId));
            return PartialView("TABuildingNameList", GetTANames(CompanyId));
        }
        [ValidateInput(false)]
        public ActionResult TABuildingListFilter()
        {
            int? CompanyId = (int?)Session["TACompanyId"];
            //return PartialView("TABuildingObjects", GetBuildingObjects(CompanyId));
            return PartialView("WebUserControl1", GetTANames(CompanyId));
        }
        [ValidateInput(false)]
        public ActionResult BuildingsObjectsBatchGridEditA([ModelBinder(typeof(DevExpressEditorsBinder))]MVCxGridViewBatchUpdateValues<BuildingObject, int> updateValues)
        //public ActionResult BuildingsObjectsBatchGridEditA([ModelBinder(typeof(DevExpressEditorsBinder))]MVCxGridViewBatchUpdateValues<TAReport, int> updateValues)
        {
            /*
            foreach (var product in updateValues.Insert)
            {
                if (updateValues.IsValid(product))
                    InsertProduct(product, updateValues);
            }*/
            foreach (var product in updateValues.Update)
            {

                //int Id = product.Id;
                int done = _buildingObjectService.EditBuilding(product.Id, product.GlobalBuilding, HostName);
                /*
                if (updateValues.IsValid(product))
                    UpdateProduct(product, updateValues);*/
            }/*
            foreach (var productID in updateValues.DeleteKeys)
            {
                DeleteProduct(productID, updateValues);
            }*/
            int? CompanyId = (int?)Session["TACompanyId"];


            return PartialView("TABoGridViewPartialView", GetBuildingObjects(CompanyId));
            //return PartialView("TABuildingObjects", GetBuildingObjects(CompanyId));
        }

        public JsonResult Checkdate()
        {

            DateTime today = DateTime.Today;

            DateTime now = DateTime.Now;
            DateTime lastDayLastMonth = new DateTime(now.Year, now.Month, 1);
            DateTime startOfMonth = new DateTime(today.Year, today.Month, 1).AddMonths(-1);
            lastDayLastMonth = lastDayLastMonth.AddDays(-1);
            string a = startOfMonth.ToShortDateString();
            string b = lastDayLastMonth.ToShortDateString();
            string sdate = Convert.ToDateTime(a).ToString("dd'.'MM'.'yyyy");
            string edate = Convert.ToDateTime(b).ToString("dd'.'MM'.'yyyy");
            var som = sdate;
            var eom = edate;
            var data = new { eom = edate, som = sdate };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult chkcurrentDate()
        {
            string from = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd.MM.yyyy");
            string to = DateTime.Now.ToString("dd.MM.yyyy");
            var som = from;
            var eom = to;

            var data = new { eom = to, som = from };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region PivotGridReport
        [HttpGet]
        public ActionResult OpenMounthReport(int format, int? department, int? company, int id, string FromDateTA, string ToDateTA)
        {

            DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",

                System.Globalization.CultureInfo.InvariantCulture);


            if (CurrentUser.Get().IsCompanyManager) { company = CurrentUser.Get().CompanyId; }
            Session["TAStartDate"] = FromDateTA;
            Session["TAStoptDate"] = ToDateTA;
            Session["TACompanyId"] = company;
            Session["TAdepartmentId"] = department;
            Session["Format"] = format;
            var tamvm = CreateViewModel<TAReportMounthViewModel>();
            if (CurrentUser.Get().IsSuperAdmin || CurrentUser.Get().IsCompanyManager || CurrentUser.Get().IsDepartmentManager)
            {
                Mapper.Map(GetTAreports(From, To, company, department), tamvm.TAReportMounthItems);
            }
            if (format == 1)
            {
                return PartialView("TAMounthReportGridViewPartialView", tamvm.TAReportMounthItems);
            }
            else { return PartialView("TAMounthReportGridViewPartialViewLast", tamvm.TAReportMounthItems); }
        }

        public ActionResult OpenMounthReportWrokingDays(int format, int? department, int? company, int id, string FromDateTA, string ToDateTA)
        {

            DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",

                System.Globalization.CultureInfo.InvariantCulture);


            if (CurrentUser.Get().IsCompanyManager) { company = CurrentUser.Get().CompanyId; }
            Session["TAStartDate"] = FromDateTA;
            Session["TAStoptDate"] = ToDateTA;
            Session["TACompanyId"] = company;
            Session["TAdepartmentId"] = department;
            Session["Format"] = format;
            var tamvm = CreateViewModel<TAReportMounthViewModel>();
            if (CurrentUser.Get().IsSuperAdmin || CurrentUser.Get().IsCompanyManager || CurrentUser.Get().IsDepartmentManager)
            {
                Mapper.Map(GetTAreports(From, To, company, department), tamvm.TAReportMounthItems);
            }
            if (format == 1)
            {
                return PartialView("TAMonthReportWorkingDaysGridViewPartialView", tamvm.TAReportMounthItems);
            }
            else { return PartialView("TAMounthReportWorkingDaysGridViewPartialViewLast", tamvm.TAReportMounthItems); }
        }

        public ActionResult OpenMounthReportUsers(int? department, int? company, int id, string FromDateTA, string ToDateTA)
        {
            if (CurrentUser.Get().IsCompanyManager) { company = CurrentUser.Get().CompanyId; }
            DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);


            DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            Session["TAStartDate"] = FromDateTA;
            Session["TAStoptDate"] = ToDateTA;
            Session["TACompanyId"] = company;
            Session["TAdepartmentId"] = department;
            var tamvm = CreateViewModel<TAReportMounthViewModel>();
            if (CurrentUser.Get().IsSuperAdmin || CurrentUser.Get().IsCompanyManager || CurrentUser.Get().IsDepartmentManager)
            {
                Mapper.Map(GetTAreports(From, To, company, department), tamvm.TAReportMounthItems);
                //   tamvm.TAReportUserTotal = _reportRepository.FindAll();
                //var tam = tamvm.TAReportMounthItems.GroupBy(x => x.UserId).ToList();
            }
            return PartialView("MounthReport", tamvm);
        }

        public ActionResult MouthReportAddTAReport(int? rowIndex, int? columnIndex/*, bool? isResetGridViewPageIndex*/)
        {
            Session["TArowIndex"] = rowIndex;
            Session["TAcolumnIndex"] = columnIndex;
            string FromDateTA = (string)Session["TAStartDate"];
            string ToDateTA = (string)Session["TAStoptDate"];
            int? CompanyId = (int?)Session["TACompanyId"];
            int? DepartmentId = (int?)Session["TAdepartmentId"];
            int? format = (int?)Session["Format"];
            DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
               System.Globalization.CultureInfo.InvariantCulture);
            DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

            object dataObject = null;
            if (rowIndex != null && columnIndex != null)
                dataObject = PivotGridExtension.CreateDrillDownDataSource(
                    PivotGridFeaturesDemosHelper.DrillDownPivotGridSettings,
                    GetTAreports(From, To, CompanyId, DepartmentId),
                    columnIndex.Value, rowIndex.Value
                );
            return PartialView("DrillDownGridViewPartial", dataObject);
        }
        public ActionResult MouthReportEditTAReport(int? rowIndex, int? columnIndex, string rowValue/*, bool? isResetGridViewPageIndex*/)
        {
            Session["TArowIndex"] = rowIndex;
            Session["TAcolumnIndex"] = columnIndex;
            string FromDateTA = (string)Session["TAStartDate"];
            string ToDateTA = (string)Session["TAStoptDate"];
            int? CompanyId = (int?)Session["TACompanyId"];
            int? DepartmentId = (int?)Session["TAdepartmentId"];
            int? format = (int?)Session["Format"];
            DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
               System.Globalization.CultureInfo.InvariantCulture);
            DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            var names = rowValue.Split(' ');
            int usr = _userRepository.FindAll(x => x.LastName == names[0] && x.FirstName == names[1] && !x.IsDeleted).First().Id;

            Session["UserId"] = usr;

            object dataObject = null;
            if (rowIndex != null && columnIndex != null)
            {
                dataObject = PivotGridExtension.CreateDrillDownDataSource(
                    PivotGridFeaturesDemosHelper.DrillDownPivotGridSettings,
                    GetTAreports(From, To, CompanyId, DepartmentId),
                    columnIndex.Value, rowIndex.Value
                );
            }
            else
            {
                dataObject = PivotGridExtension.CreateDrillDownDataSource(
                 PivotGridFeaturesDemosHelper.DrillDownPivotGridSettings,
                 GetTAreports(From, To, usr));
            }
            ViewBag.TotalRowsCount = dataObject;

            return PartialView("DrillDownGridViewPartial", dataObject);
        }
        [ValidateInput(false)]
        public ActionResult TAReportObjectsBatchGridEdit([ModelBinder(typeof(DevExpressEditorsBinder))]MVCxGridViewBatchUpdateValues<TAReportItem, int> updateValues)
        {
            string result1 = "Date already exist please edit record";
            int usr = (int)Session["UserId"];
            if (updateValues.Insert.Count() != 0)
            {
                User user = _userRepository.FindById(usr);
                try
                {
                    int? departmentId = user.UserDepartments.First().DepartmentId;

                    foreach (var product in updateValues.Insert)
                    {

                        string name = product.ReportDate.DayOfWeek.ToString().Substring(0, 3) + " " + product.ReportDate.Day.ToString();

                        TimeSpan interval = TimeSpan.Parse(product.Hours_Min);
                        string date = product.ReportDate.ToShortDateString().ToString();
                        float hours = (float)interval.TotalSeconds;
                        var result = _reportRepository.FindAll(x => x.ReportDate == Convert.ToDateTime(date) && x.UserId == usr && !x.IsDeleted).Any();
                        if (result == true)
                        {
                            return Json(result1);

                        }
                        else
                        {
                            _TAReportService.CreateTAReport(usr, departmentId, name, product.ReportDate.Date, Int16.Parse(product.ReportDate.Day.ToString()), hours, 1, 2, true, false);
                        }
                    }
                }
                catch (Exception ee)
                {

                    foreach (var product in updateValues.Insert)
                    {
                        string name = product.ReportDate.DayOfWeek.ToString().Substring(0, 3) + " " + product.ReportDate.Day.ToString();
                        TimeSpan interval = TimeSpan.Parse(product.Hours_Min);
                        string date = product.ReportDate.ToShortDateString().ToString();
                        float hours = (float)interval.TotalSeconds;
                        var result = _reportRepository.FindAll(x => x.ReportDate == Convert.ToDateTime(date) && x.UserId == usr && !x.IsDeleted).Any();
                        //var q = _reportRepository.FindAll();
                        if (result == true)
                        {
                            return Json(result1);

                        }
                        else
                        {

                            _TAReportService.CreateTAReport(usr, null, name, product.ReportDate.Date, Int16.Parse(product.ReportDate.Day.ToString()), hours, 1, 2, true, false);
                        }
                        if (usr < 0)
                        {
                            throw ee;
                        }
                    }

                }

            }
            foreach (var product in updateValues.Update)
            {
                TimeSpan interval = TimeSpan.Parse(product.Hours_Min);

                product.Hours = (float)interval.TotalSeconds;
                _TAReportService.EditTAReport(product.Id, product.Hours);
            }

            foreach (var productID in updateValues.DeleteKeys)
            {
                _TAReportService.DeleteTAReport(productID);
            }

            int? rowIndex = (int?)Session["TArowIndex"];
            int? columnIndex = (int?)Session["TAcolumnIndex"];
            string FromDateTA = (string)Session["TAStartDate"];
            string ToDateTA = (string)Session["TAStoptDate"];
            int? CompanyId = (int?)Session["TACompanyId"];
            int? DepartmentId = (int?)Session["TAdepartmentId"];
            int? format = (int?)Session["Format"];
            DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
               System.Globalization.CultureInfo.InvariantCulture);
            DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",
                 System.Globalization.CultureInfo.InvariantCulture);

            object dataObject = null;
            if (rowIndex != null && columnIndex != null)
            {
                dataObject = PivotGridExtension.CreateDrillDownDataSource(
                    PivotGridFeaturesDemosHelper.DrillDownPivotGridSettings,
                    GetTAreports(From, To, CompanyId, DepartmentId),
                    columnIndex.Value, rowIndex.Value
                );
            }
            else
            {
                dataObject = PivotGridExtension.CreateDrillDownDataSource(
                 PivotGridFeaturesDemosHelper.DrillDownPivotGridSettings,
                 GetTAreports(From, To, usr));
            }

            return PartialView("DrillDownGridViewPartial", dataObject);
        }
        [ValidateInput(false)]
        public ActionResult TAMounthReportGridViewPartialA()
        {
            string FromDateTA = (string)Session["TAStartDate"];
            string ToDateTA = (string)Session["TAStoptDate"];
            int? CompanyId = (int?)Session["TACompanyId"];
            int? DepartmentId = (int?)Session["TAdepartmentId"];
            int? format = (int?)Session["Format"];
            DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
                           System.Globalization.CultureInfo.InvariantCulture);
            DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            var tamvm = CreateViewModel<TAReportMounthViewModel>();
            Mapper.Map(GetTAreports(From, To, CompanyId, DepartmentId), tamvm.TAReportMounthItems);
            if (format == 1)
            {
                return PartialView("TAMounthReportGridViewPartialView", tamvm.TAReportMounthItems);
            }
            else { return PartialView("TAMounthReportGridViewPartialViewLast", tamvm.TAReportMounthItems); }
        }
        #endregion
        #region MyReport
        [HttpGet]
        public ActionResult MyTAReport(int format, int? departmentId, int? company, int id, string FromDateTA, string ToDateTA)
        {
            id = 1119;// CurrentUser.Get().Id;
            //int companyid = 0;
            DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

            Session["TAStartDate"] = FromDateTA;
            Session["TAStoptDate"] = ToDateTA;
            Session["TACompanyId"] = company;
            Session["TAdepartmentId"] = departmentId;
            Session["Format"] = format;
            var tamvm = CreateViewModel<TAMsUserViewModel>();
            //var tamvm = CreateViewModel<TAMoveListViewModel>();
            // Mapper.Map(GetUserTAMoves(FromDateTA, ToDateTA, id), tamvm.TAMsUserItems);
            tamvm.TAMsUserItems = GetUserTAMoves(id, From, To); // _reportRepository.FindAll();
            //var tam = tamvm.TAReportMounthItems.GroupBy(x => x.UserId).ToList();
            //var tamvm = GetUserTAMoves(FromDateTA, ToDateTA, id);
            return PartialView("TAMsUserReport", tamvm.TAMsUserItems);
            //return PartialView("OpenMounthReportUser", tamvm);

        }

        [ValidateInput(false)]
        public ActionResult MyTAReportGridViewPartialA()
        {
            string StartDate = (string)Session["TAStartDate"];
            string StoptDate = (string)Session["TAStoptDate"];
            int? CompanyId = (int?)Session["TACompanyId"];
            int? format = (int?)Session["Format"];
            DateTime From = DateTime.ParseExact(StartDate, "dd.MM.yyyy",
                           System.Globalization.CultureInfo.InvariantCulture);
            DateTime To = DateTime.ParseExact(StoptDate, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);


            int id = 1119;// CurrentUser.Get().Id;
            var tamvm = CreateViewModel<TAMsUserViewModel>();
            tamvm.TAMsUserItems = GetUserTAMoves(id, From, To); // _reportRepository.FindAll();
            return PartialView("TAMsUserReport", tamvm.TAMsUserItems);
        }
        #endregion
        #region DetailUsersReport

        public ActionResult AddNew(string a)
        {
            string StartDate = (string)Session["TAStartDate"];
            string StoptDate = (string)Session["TAStoptDate"];
            int? company = (int?)Session["TACompanyId"];
            int? department = (int?)Session["TAdepartmentId"];

            DateTime From = DateTime.ParseExact(StartDate, "dd.MM.yyyy",
                                        System.Globalization.CultureInfo.InvariantCulture);


            DateTime To = DateTime.ParseExact(StoptDate, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

            var tamvm = CreateViewModel<TAMsUserViewModel>();
            if (CurrentUser.Get().IsSuperAdmin || CurrentUser.Get().IsCompanyManager || CurrentUser.Get().IsDepartmentManager)
            {


                tamvm.TAMsUserItems = GetUsersTAMoves(From, To, company, department, 0); // _reportRepository.FindAll();


                //   tamvm.TAReportUserTotal = _reportRepository.FindAll();
                //var tam = tamvm.TAReportMounthItems.GroupBy(x => x.UserId).ToList();
            }
            return PartialView("TAReportDetailedGridViewPartialView", tamvm.TAMsUserItems);
        }

        [ValidateInput(false)]
        public ActionResult TAReportDetailedGrid()
        {

            string StartDate = (string)Session["TAStartDate"];
            string StoptDate = (string)Session["TAStoptDate"];
            int? company = (int?)Session["TACompanyId"];
            int? department = (int?)Session["TAdepartmentId"];


            DateTime From = DateTime.ParseExact(StartDate, "dd.MM.yyyy",
                                        System.Globalization.CultureInfo.InvariantCulture);


            DateTime To = DateTime.ParseExact(StoptDate, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

            var tamvm = CreateViewModel<TAMsUserViewModel>();
            if (CurrentUser.Get().IsSuperAdmin || CurrentUser.Get().IsCompanyManager || CurrentUser.Get().IsDepartmentManager)
            {


                tamvm.TAMsUserItems = GetUsersTAMoves(From, To, company, department, 0); // _reportRepository.FindAll();


                //   tamvm.TAReportUserTotal = _reportRepository.FindAll();
                //var tam = tamvm.TAReportMounthItems.GroupBy(x => x.UserId).ToList();
            }
            return PartialView("TAReportDetailedGridViewPartialView", tamvm.TAMsUserItems);
        }
        #endregion
        #region StartEnd
        [HttpGet]
        public ActionResult StartEndReport(int format, int? department, int? company, int id, string FromDateTA, string ToDateTA)
        {
            DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            if (CurrentUser.Get().IsCompanyManager) { company = CurrentUser.Get().CompanyId; }
            Session["TAStartDate"] = FromDateTA;
            Session["TAStoptDate"] = ToDateTA;
            Session["TACompanyId"] = company;
            Session["TAdepartmentId"] = department;
            Session["Format"] = format;
            var tamvm = CreateViewModel<TAMsUserViewModel>();
            if (CurrentUser.Get().IsSuperAdmin || CurrentUser.Get().IsCompanyManager || CurrentUser.Get().IsDepartmentManager)
            {


                tamvm.TAMsUserItems = GetUsersTAMoves(From, To, company, department, 0); // _reportRepository.FindAll();



            }

            return PartialView("TAMounthReportGridViewPartialView1", tamvm.TAMsUserItems);

        }




        [ValidateInput(false)]
        public ActionResult TAStartEnd([ModelBinder(typeof(DevExpressEditorsBinder))]MVCxGridViewBatchUpdateValues<TAMove, int> updateValues)
        {
            foreach (var product in updateValues.Insert)
            {
                if (updateValues.IsValid(product))
                    InsertProduct(product, updateValues);
            }
            foreach (var product in updateValues.Update)
            {
                if (updateValues.IsValid(product))
                    UpdateProduct(product, updateValues);
            }
            foreach (var ID in updateValues.DeleteKeys)
            {
                _TAMoveService.DeleteTAMove(ID);
            }


            string StartDate = (string)Session["TAStartDate"];
            string StoptDate = (string)Session["TAStoptDate"];
            int? CompanyId = (int?)Session["TACompanyId"];
            int? department = (int?)Session["TAdepartmentId"];
            DateTime From = DateTime.ParseExact(StartDate, "dd.MM.yyyy",
                           System.Globalization.CultureInfo.InvariantCulture);


            DateTime To = DateTime.ParseExact(StoptDate, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

            return PartialView("---", GetUsersTAMoves(From, To, CompanyId, department, 0));
        }
        [HttpGet]
        public ActionResult TAReportDetailed(int? department, int? company, int id, string FromDateTA, string ToDateTA, int? BuildingId)
        {

            DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);


            DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);



            Session["UsersTA"] = _userRepository.FindAll(x => x.WorkTime == true && !x.IsDeleted);

            Session["TAStartDate"] = FromDateTA;
            Session["TAStoptDate"] = ToDateTA;
            Session["TACompanyId"] = company;
            var tamvm = CreateViewModel<TAMsUserViewModel>();
            if (CurrentUser.Get().IsSuperAdmin || CurrentUser.Get().IsCompanyManager || CurrentUser.Get().IsDepartmentManager)
            {

                if (CurrentUser.Get().IsCompanyManager)
                {


                    company = CurrentUser.Get().CompanyId;


                    tamvm.TAMsUserItems = GetUsersTAMoves(From, To, company, department, BuildingId);

                }
                else
                {
                    tamvm.TAMsUserItems = GetUsersTAMoves(From, To, company, department, BuildingId); // _reportRepository.FindAll();
                }



            }
            return PartialView("TAReportDetailed", tamvm);
        }

        [ValidateInput(false)]
        public ActionResult TAReportDetailedGridEditA([ModelBinder(typeof(DevExpressEditorsBinder))]MVCxGridViewBatchUpdateValues<TAMove, int> updateValues)
        {
            foreach (var product in updateValues.Insert)
            {
                if (updateValues.IsValid(product))
                    InsertProduct(product, updateValues);
            }
            foreach (var product in updateValues.Update)
            {
                if (updateValues.IsValid(product))
                    UpdateProduct(product, updateValues);
            }
            foreach (var ID in updateValues.DeleteKeys)
            {
                _TAMoveService.DeleteTAMove(ID);
            }


            string StartDate = (string)Session["TAStartDate"];
            string StoptDate = (string)Session["TAStoptDate"];
            int? CompanyId = (int?)Session["TACompanyId"];
            int? department = (int?)Session["TAdepartmentId"];
            DateTime From = DateTime.ParseExact(StartDate, "dd.MM.yyyy",
                           System.Globalization.CultureInfo.InvariantCulture);


            DateTime To = DateTime.ParseExact(StoptDate, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

            return PartialView("TAMounthReportGridViewPartialView", GetTAreports(From, To, CompanyId, department));
        }
        [ValidateInput(false)]
        public void ExternalEditFormDelete(int? Id)
        {
            if (Id != null)
            {
                _TAMoveService.DeleteTAMove(Id.GetValueOrDefault());
            }
        }

        public ActionResult DrillDownGridViewPartial()
        {
            return PartialView("TAReportDetailed");
        }



        public PdfExportOptions GetPdfExportOptions()
        {
            var exportOptions = new PdfExportOptions();
            exportOptions.DocumentOptions.Title = "Test";
            exportOptions.DocumentOptions.Subject = "Subject";
            exportOptions.DocumentOptions.Author = "Author";
            return exportOptions;
        }
        private GridViewSettings GetPivotSettingsNewBuildingForEnglish(int? timeFormat)
        {

            var settings = new GridViewSettings();

            string StartDate = (string)Session["TAStartDate"];
            string StoptDate = (string)Session["TAStoptDate"];


            int department = Convert.ToInt32(Session["department"]);
            int company = Convert.ToInt32(Session["company"]);



            //  var qry = _UserDepartmentRepository.FindAll(x => x.Id == department).SingleOrDefault();

            var qry = db.Companies.Where(x => x.Id == company).SingleOrDefault();

            if (qry != null)
            {

                cname = qry.Name;
            }
            var qry1 = db.Departments.Where(x => x.Id == department).SingleOrDefault();

            if (qry1 != null)
            {
                depname = qry1.Name;
            }



            settings.Name = "GridView";
            settings.CallbackRouteValues = new { Controller = "Home", Action = "GridViewPartial" };

            // Export-specific settings  
            // settings.SettingsExport.ExportedRowType = DevExpress.Web.Export.GridViewExportedRowType.All;
            settings.SettingsExport.FileName = "Report.xls";
            settings.SettingsExport.PaperKind = System.Drawing.Printing.PaperKind.A4;
            settings.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            settings.ControlStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            settings.KeyFieldName = "id";
            settings.Settings.ShowFilterRow = true;
            settings.SettingsExport.Styles.Header.Font.Bold = true;

            //settings.Columns.Add("User.LastName");
            //settings.Columns.Add("User.FirstName");
            settings.Columns.Add(column =>
            {
                //column.FieldName = "User.LastName";
                column.FieldName = "FullName";
                // column.Caption = null;
                //column.Caption = "Name";
                //settings.SettingsExport.Styles.Cell.Font.Bold = true;

                //settings.SettingsExport.Styles.Header.Font.Bold = true;


                column.Caption = "Full Name";

                column.GroupIndex = 1;
                column.Width = 50;
                // column.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;

                column.Visible = false;
            });

            settings.Columns.Add(column =>
            {
                //column.FieldName = "User.LastName";
                column.FieldName = "companydeatils";


                column.Caption = "Company Details";

                column.Visible = false;
                //column.Caption = "Name";
                column.GroupIndex = 0;
                column.Width = 50;
                // column.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;


            });
            settings.Columns.Add(column =>
            {

                column.FieldName = "FirstName";

                column.Caption = "First Name";
                column.CellStyle.Font.Bold = true;
                //settings.SettingsExport.Styles.Cell.Font.Bold = true;
                //settings.SettingsExport.Styles.Cell.Font.Bold = true;

                //settings.SettingsExport.Styles.Header.Font.Bold = true;


            });


            settings.Columns.Add(column =>
            {
                column.FieldName = "LastName";

                column.Caption = "Last Name";
                //settings.SettingsExport.Styles.Cell.Font.Bold = true;

                //settings.SettingsExport.Styles.Header.Font.Bold = true;


            });



            settings.Columns.Add(column =>
            {
                column.FieldName = "PersonalCode";

                column.Caption = "Personal Code";
                column.CellStyle.Font.Bold = true;
                // settings.SettingsExport.Styles.Cell.Font.Bold = true;
                settings.SettingsExport.Styles.Cell.Font.Bold = true;

                //settings.SettingsExport.Styles.Header.Font.Bold = true;

                // column.PropertiesEdit.DisplayFormatString = "d";

            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "Name";

                column.Caption = "Designation";
                //settings.SettingsExport.Styles.Cell.Font.Bold = true;

                //settings.SettingsExport.Styles.Header.Font.Bold = true;


            });
            settings.SettingsExport.Styles.Cell.Font.Bold = true;

            settings.SettingsExport.Styles.Header.Font.Bold = true;

            settings.Columns.Add(column =>
            {
                column.FieldName = "Started";
                column.Caption = "Started";
                //settings.SettingsExport.Styles.Header.Font.Bold = true;
                column.PropertiesEdit.DisplayFormatString = "dddd M/d/yyyy ";
                column.Width = 200;

            });
            //settings.Columns.Add(column =>
            //{
            //    column.FieldName = "Started";
            //    column.Caption = " ";
            //    // column.PropertiesEdit.DisplayFormatString = "d";
            //    column.PropertiesEdit.DisplayFormatString = "HH:mm";
            //    column.Width = 50;
            //});
            settings.Columns.Add(column =>
            {
                column.FieldName = "checkin";
                column.Caption = "Check In";
                // column.PropertiesEdit.DisplayFormatString = "d";
                //settings.SettingsExport.Styles.Header.Font.Bold = true;
                //settings.SettingsExport.Styles.Header.Font.Bold = true;
                column.Width = 30;
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "checkout";
                column.Caption = "Check Out";
                // column.PropertiesEdit.DisplayFormatString = "d";
                column.Width = 30;

            });
            settings.SettingsExport.Styles.Header.Font.Bold = true;
            settings.Columns.Add(column =>
            {
                column.FieldName = "CompnyId";
                //column.GroupIndex = 0;
                column.Visible = false;
                // column.PropertiesEdit.DisplayFormatString = "d";

            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "Hours";

                column.Visible = false;
                // column.PropertiesEdit.DisplayFormatString = "d";

            });

            //settings.Columns.Add(column =>
            //{
            //    column.FieldName = "companycount";

            //    column.Visible = false;
            //    // column.PropertiesEdit.DisplayFormatString = "d";

            //});
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "Hours");
            //settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "totaltime").ShowInGroupFooterColumn = "totaltime";
            //settings.SettingsExport.Styles.Header.Font.Bold = true;
            settings.Settings.ShowGroupedColumns = true;
            settings.Columns.Add(column =>
            {
                column.FieldName = "totaltime";
                column.Caption = "Total Time";
                column.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                column.Width = 30;
                //column.PropertiesEdit.DisplayFormatString = "HH:mm";
                //column.PropertiesEdit.NullDisplayText = " ";
                //settings.SettingsExport.Styles.Header.Font.Bold = true;
            });


            settings.SummaryDisplayText = (sender, e) =>
            {
                settings.Styles.Cell.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
                settings.SettingsExport.Styles.Header.Font.Bold = true;
                if (e.IsGroupSummary)
                {
                    if (e.Item.FieldName == "Hours")
                    {


                        double time = Double.Parse(e.Value.ToString());
                        string hours = Math.Floor((TimeSpan.FromSeconds(time)).TotalHours) > 9 ? Math.Floor((TimeSpan.FromSeconds(time)).TotalHours).ToString() : "0" + Math.Floor((TimeSpan.FromSeconds(time)).TotalHours).ToString();
                        string minutes = (TimeSpan.FromSeconds(time)).Minutes > 9 ? (TimeSpan.FromSeconds(time)).Minutes.ToString() : "0" + (TimeSpan.FromSeconds(time)).Minutes;
                        //e.Text = "a month together: = " + hours + ":" + minutes;
                        string val1 = "Total Hours: = " + hours + ":" + minutes;
                        string te = val1.Replace(",", "");
                        e.Text = te;
                        //tt = e.Text;


                    }
                }
            };

            //settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "totaltime").ShowInGroupFooterColumn = "totaltime";
            //settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Count, "totaltime").ShowInGroupFooterColumn = "totaltime";
            //string companytoal = "";
            //settings.Settings.ShowFooter = true;
            settings.Settings.ShowGroupFooter = GridViewGroupFooterMode.VisibleIfExpanded;
            settings.Settings.ShowGroupPanel = true;

            //int temp = 0; int i = 0;


            settings.Settings.ShowGroupPanel = true;
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Custom, "totaltime").DisplayFormat = "c";
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Custom, "totaltime").ShowInGroupFooterColumn = "totaltime";
            settings.SettingsBehavior.AutoExpandAllGroups = true;

            return settings;


        }
        private GridViewSettings GetPivotSettingsNewBuilding(int? timeFormat)
        {

            var settings = new GridViewSettings();

            string StartDate = (string)Session["TAStartDate"];
            string StoptDate = (string)Session["TAStoptDate"];


            int department = Convert.ToInt32(Session["department"]);
            int company = Convert.ToInt32(Session["company"]);



            //  var qry = _UserDepartmentRepository.FindAll(x => x.Id == department).SingleOrDefault();

            var qry = db.Companies.Where(x => x.Id == company).SingleOrDefault();

            if (qry != null)
            {

                cname = qry.Name;
            }
            var qry1 = db.Departments.Where(x => x.Id == department).SingleOrDefault();

            if (qry1 != null)
            {
                depname = qry1.Name;
            }



            settings.Name = "GridView";
            settings.CallbackRouteValues = new { Controller = "Home", Action = "GridViewPartial" };

            // Export-specific settings  
            // settings.SettingsExport.ExportedRowType = DevExpress.Web.Export.GridViewExportedRowType.All;
            settings.SettingsExport.FileName = "Report.xls";
            settings.SettingsExport.PaperKind = System.Drawing.Printing.PaperKind.A4;
            settings.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            settings.ControlStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            settings.KeyFieldName = "id";
            settings.Settings.ShowFilterRow = true;
            //settings.Columns.Add("User.LastName");
            //settings.Columns.Add("User.FirstName");
            settings.Columns.Add(column =>
            {
                //column.FieldName = "User.LastName";
                column.FieldName = "FullName";
                // column.Caption = null;
                //column.Caption = "Name";

                column.Caption = "Kopā";



                column.GroupIndex = 1;
                column.Width = 30;
                // column.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;

                column.Visible = false;
            });

            settings.Columns.Add(column =>
            {
                //column.FieldName = "User.LastName";
                column.FieldName = "companydeatils";

                column.Caption = "Uzņēmuma dati";

                column.Visible = false;
                //column.Caption = "Name";
                column.GroupIndex = 0;
                column.Width = 50;
                // column.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;


            });
            settings.Columns.Add(column =>
            {

                column.FieldName = "FirstName";
                column.Caption = "Vārds";

                // column.PropertiesEdit.DisplayFormatString = "d";

            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "LastName";

                column.Caption = "Uzvārds";

                // column.PropertiesEdit.DisplayFormatString = "d";

            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "PersonalCode";

                column.Caption = "Personas kods";

                // column.PropertiesEdit.DisplayFormatString = "d";

            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "Name";

                column.Caption = "Profesija, amats";

                // column.PropertiesEdit.DisplayFormatString = "d";

            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "Started";
                column.Caption = "Datums";
                column.PropertiesEdit.DisplayFormatString = "dddd M/d/yyyy ";
                column.Width = 200;

            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "checkin";
                column.Caption = "Iegāja";
                // column.PropertiesEdit.DisplayFormatString = "d";
                column.Width = 30;
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "checkout";
                column.Caption = "Izgāja";
                // column.PropertiesEdit.DisplayFormatString = "d";
                column.Width = 30;
            });

            settings.Columns.Add(column =>
            {
                column.FieldName = "CompnyId";
                //column.GroupIndex = 0;
                column.Visible = false;
                // column.PropertiesEdit.DisplayFormatString = "d";

            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "Hours";

                column.Visible = false;
                // column.PropertiesEdit.DisplayFormatString = "d";

            });

            //settings.Columns.Add(column =>
            //{
            //    column.FieldName = "companycount";

            //    column.Visible = false;
            //    // column.PropertiesEdit.DisplayFormatString = "d";

            //});
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "Hours");
            //settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "totaltime").ShowInGroupFooterColumn = "totaltime";

            settings.Settings.ShowGroupedColumns = true;
            settings.Columns.Add(column =>
            {
                column.FieldName = "totaltime";
                column.Caption = "Kopā";
                column.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                //column.PropertiesEdit.DisplayFormatString = "HH:mm";
                //column.PropertiesEdit.NullDisplayText = " ";
                column.Width = 30;
            });


            settings.SummaryDisplayText = (sender, e) =>
            {
                settings.Styles.Cell.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
                if (e.IsGroupSummary)
                {
                    if (e.Item.FieldName == "Hours")
                    {


                        double time = Double.Parse(e.Value.ToString());
                        string hours = Math.Floor((TimeSpan.FromSeconds(time)).TotalHours) > 9 ? Math.Floor((TimeSpan.FromSeconds(time)).TotalHours).ToString() : "0" + Math.Floor((TimeSpan.FromSeconds(time)).TotalHours).ToString();
                        string minutes = (TimeSpan.FromSeconds(time)).Minutes > 9 ? (TimeSpan.FromSeconds(time)).Minutes.ToString() : "0" + (TimeSpan.FromSeconds(time)).Minutes;
                        //e.Text = "a month together: = " + hours + ":" + minutes;
                        string val1 = "Kopā: = " + hours + ":" + minutes;
                        string te = val1.Replace(",", "");
                        e.Text = te;
                        //tt = e.Text;


                    }
                }
            };

            settings.Settings.ShowGroupFooter = GridViewGroupFooterMode.VisibleIfExpanded;
            settings.Settings.ShowGroupPanel = true;
            settings.Settings.ShowGroupPanel = true;
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Custom, "totaltime").DisplayFormat = "c";
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Custom, "totaltime").ShowInGroupFooterColumn = "totaltime";
            settings.SettingsBehavior.AutoExpandAllGroups = true;

            return settings;
        }

        private PivotGridSettings GetPivotSettings(int? timeFormat)
        {
            var settings = new PivotGridSettings();
            settings.Name = "pivotGrid";
            settings.CallbackRouteValues = new { Controller = "TAReport", Action = "TAMounthReportGridViewPartialA" };
            //settings.CustomActionRouteValues = new { Controller = "TAReport", Action = "MounthBatchGridEditA" };
            settings.OptionsView.VerticalScrollBarMode = ScrollBarMode.Auto;
            settings.OptionsView.HorizontalScrollBarMode = ScrollBarMode.Auto;

            settings.ControlStyle.Border.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
            settings.Styles.CellStyle.Border.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
            settings.StylesPager.Pager.Border.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(1);
            settings.ControlStyle.BorderBottom.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(2);

            settings.SettingsExport.OptionsPrint.PageSettings.Landscape = true;//.PaperKind = System.Drawing.Printing.PaperKind.A4;
            settings.SettingsExport.OptionsPrint.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A3Extra;
            settings.ControlStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            settings.OptionsPager.Visible = false;
            settings.OptionsFilter.NativeCheckBoxes = true;

            //settings.OptionsView.ShowColumnTotals = false;
            //settings.OptionsView.ShowCustomTotalsForSingleValues = false;
            settings.OptionsView.ShowAllTotals();
            //settings.OptionsView.HideAllTotals();


            settings.Fields.Add(field =>
            {
                field.Area = PivotArea.RowArea;
                field.Caption = "Last Name";
                field.FieldName = "User.LastName";
            });
            settings.Fields.Add(field =>
            {
                field.Area = PivotArea.RowArea;

                field.Caption = "Name";
                field.FieldName = "User.FirstName";
            });

            settings.Fields.Add(field =>
            {
                field.Area = PivotArea.ColumnArea;

                field.FieldName = "ReportDate";
                field.Caption = "ReportDate";
                field.GroupInterval = PivotGroupInterval.DateMonth;
            });

            settings.Fields.Add(field =>
            {
                field.Area = PivotArea.ColumnArea;
                field.FieldName = "ReportDate";
                field.Caption = "Quarter";
                field.GroupInterval = PivotGroupInterval.DateDay;
            });
            /*
            settings.Fields.Add(field =>
            {

                field.Area = PivotArea.DataArea;
                field.FieldName = "ValidityPeriods";
                //field.AreaIndex = 1;


                field.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                field.TotalValueFormat.FormatString = "{0:n2}";

                //field.SummaryType = PivotSummaryType.Max;
                field.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                //field.CellFormat.FormatString = "{0:hh\\:mm}";
            });
            */

            settings.Fields.Add(field =>
            {
                field.Area = PivotArea.DataArea;
                field.FieldName = "Hours";
            });
            if (timeFormat == 1)
            {
                settings.CustomCellDisplayText = (sender, e) =>
                {

                    if (e.ColumnValueType == PivotGridValueType.Total || e.ColumnValueType == PivotGridValueType.GrandTotal)
                    {
                        double time1 = Convert.ToDouble(e.Value);
                        string hours = Math.Floor((TimeSpan.FromSeconds(time1)).TotalHours) > 9 ? Math.Floor((TimeSpan.FromSeconds(time1)).TotalHours).ToString() : "0" + Math.Floor((TimeSpan.FromSeconds(time1)).TotalHours).ToString();
                        string minutes = (TimeSpan.FromSeconds(time1)).Minutes > 9 ? (TimeSpan.FromSeconds(time1)).Minutes.ToString() : "0" + (TimeSpan.FromSeconds(time1)).Minutes;
                        e.DisplayText = hours + ":" + minutes;

                        return;
                    }
                    double time = Convert.ToDouble(e.Value);
                    if (time == 0) return;
                    e.DisplayText = DateTime.MinValue.Add(TimeSpan.FromSeconds(time)).ToString("HH:mm");
                };
            }
            else
            {
                settings.CustomCellDisplayText = (sender, e) =>
                {
                    double time = Convert.ToDouble(e.Value);
                    if (time == 0) return;
                    double time2 = Math.Floor((TimeSpan.FromSeconds(Convert.ToDouble(e.Value))).TotalSeconds);
                    e.DisplayText = (time2 / 3600).ToString("F2");
                };
            }
            return settings;

        }

        private GridViewSettings GetGridSettingsExcels(int? timeFormat)
        {

            var settings = new GridViewSettings();

            //---> my
            string StartDate = (string)Session["TAStartDate"];
            string StoptDate = (string)Session["TAStoptDate"];


            int department = Convert.ToInt32(Session["department"]);
            int company = Convert.ToInt32(Session["company"]);



            //  var qry = _UserDepartmentRepository.FindAll(x => x.Id == department).SingleOrDefault();

            var qry = db.Companies.Where(x => x.Id == company).SingleOrDefault();

            if (qry != null)
            {

                cname = qry.Name;
            }
            var qry1 = db.Departments.Where(x => x.Id == department).SingleOrDefault();

            if (qry1 != null)
            {
                depname = qry1.Name;
            }


            settings.SettingsExport.PageHeader.Center = depname + " - " + cname + " ";


            settings.SettingsExport.PageHeader.Left = StartDate + " - " + StoptDate + " ";
            settings.SettingsExport.PageHeader.Right = DateTime.Now.Date.ToString("dd.MM.yyyy");
            //<--- my
            settings.Name = "GridView";
            settings.CallbackRouteValues = new { Controller = "Home", Action = "GridViewPartial" };

            // Export-specific settings  
            // settings.SettingsExport.ExportedRowType = DevExpress.Web.Export.GridViewExportedRowType.All;
            settings.SettingsExport.FileName = "Report.xls";
            settings.SettingsExport.PaperKind = System.Drawing.Printing.PaperKind.A4;
            settings.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            settings.ControlStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            settings.KeyFieldName = "Id";
            //settings.Columns.Add("User.LastName");
            // settings.Columns.Add("User.FirstName");

            settings.Columns.Add(column =>
            {
                //column.FieldName = "User.LastName";
                column.FieldName = "UserId";
                // column.Caption = null;
                column.Caption = "UserId";
                column.GroupIndex = 0;
                column.Width = 50;
                column.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;

                //column.Visible = false;
            });

            settings.Columns.Add("UserId");
            //settings.Columns.Add("User.FirstName");

            settings.Columns.Add(column =>
            {
                column.FieldName = "Started";
                column.Caption = " ";
                // column.PropertiesEdit.DisplayFormatString = "d";
                column.PropertiesEdit.DisplayFormatString = "dddd M/d/yyyy ";
                column.Width = 200;
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "Started";
                column.Caption = " ";
                // column.PropertiesEdit.DisplayFormatString = "d";
                column.PropertiesEdit.DisplayFormatString = "HH:mm";
                column.Width = 50;
            });
            //toTime
            settings.Columns.Add(column =>
            {
                column.FieldName = "Finished";
                column.Caption = " ";
                column.PropertiesEdit.DisplayFormatString = "d";
                column.PropertiesEdit.DisplayFormatString = "HH:mm";
                column.Width = 50;
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "Hours";
                column.Visible = false;
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "colorflag";
                column.Visible = false;

            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "Total";
                column.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                column.PropertiesEdit.DisplayFormatString = "HH:mm";
                column.PropertiesEdit.NullDisplayText = " ";
            });

            settings.Columns.Add(column =>
            {
                column.FieldName = "User.LastName";
                column.Caption = "Comment";
            });

            settings.CustomUnboundColumnData = (sender, e) =>
            {
                if (e.Column.FieldName == "Total")
                {
                    double time = Convert.ToDouble(e.GetListSourceFieldValue("Hours"));
                    if (time == 0) { e.Value = null; }
                    else
                    {
                        e.Value = DateTime.MinValue.Add(TimeSpan.FromSeconds(time));
                    }
                }
            };

            //Summary
            settings.Settings.ShowFooter = true;


            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "Hours");

            settings.SummaryDisplayText = (sender, e) =>
            {
                if (e.IsGroupSummary)
                {
                    if (e.Item.FieldName == "Hours")
                    {
                        double time = Double.Parse(e.Value.ToString());
                        string hours = Math.Floor((TimeSpan.FromSeconds(time)).TotalHours) > 9 ? Math.Floor((TimeSpan.FromSeconds(time)).TotalHours).ToString() : "0" + Math.Floor((TimeSpan.FromSeconds(time)).TotalHours).ToString();
                        string minutes = (TimeSpan.FromSeconds(time)).Minutes > 9 ? (TimeSpan.FromSeconds(time)).Minutes.ToString() : "0" + (TimeSpan.FromSeconds(time)).Minutes;
                        e.Text = "Total hours = " + hours + ":" + minutes;
                    }
                }
            };

            return settings;
        }

        private GridViewSettings GetGridSettings(int? timeFormat)
        {

            var settings = new GridViewSettings();

            //---> my
            string StartDate = (string)Session["TAStartDate"];
            string StoptDate = (string)Session["TAStoptDate"];


            int department = Convert.ToInt32(Session["department"]);
            int company = Convert.ToInt32(Session["company"]);




            var qry = db.Companies.Where(x => x.Id == company).SingleOrDefault();

            if (qry != null)
            {

                cname = qry.Name;
            }
            var qry1 = db.Departments.Where(x => x.Id == department).SingleOrDefault();

            if (qry1 != null)
            {
                depname = qry1.Name;
            }


            settings.SettingsExport.PageHeader.Center = depname + " - " + cname + " ";


            settings.SettingsExport.PageHeader.Left = StartDate + " - " + StoptDate + " ";
            settings.SettingsExport.PageHeader.Right = DateTime.Now.Date.ToString("dd.MM.yyyy");
            //<--- my
            settings.Name = "GridView";
            settings.CallbackRouteValues = new { Controller = "Home", Action = "GridViewPartial" };


            settings.SettingsExport.FileName = "Report.pdf";
            settings.SettingsExport.PaperKind = System.Drawing.Printing.PaperKind.A4;
            settings.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            settings.ControlStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            settings.KeyFieldName = "Id";

            settings.Columns.Add(column =>
            {
                //column.FieldName = "User.LastName";
                column.FieldName = "UserId";
                // column.Caption = null;
                column.Caption = "UserId";
                column.GroupIndex = 0;
                column.Width = 50;
                column.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;

                //column.Visible = false;
            });

            settings.Columns.Add("User.LastName");
            settings.Columns.Add("User.FirstName");

            settings.Columns.Add(column =>
            {
                column.FieldName = "Started";
                column.Caption = "Started";
                // column.PropertiesEdit.DisplayFormatString = "d";
                column.PropertiesEdit.DisplayFormatString = "dddd M/d/yyyy ";
                column.Width = 200;
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "Started";
                column.Caption = "Check In";
                // column.PropertiesEdit.DisplayFormatString = "d";
                column.PropertiesEdit.DisplayFormatString = "HH:mm";
                column.Width = 50;
            });
            //toTime
            settings.Columns.Add(column =>
            {
                column.FieldName = "Finished";
                column.Caption = "Check Out";
                column.PropertiesEdit.DisplayFormatString = "d";
                column.PropertiesEdit.DisplayFormatString = "HH:mm";
                column.Width = 50;
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "Hours";
                column.Visible = false;
            });
            settings.Columns.Add(column =>
            {
                column.FieldName = "Total";
                column.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
                column.PropertiesEdit.DisplayFormatString = "HH:mm";
                column.PropertiesEdit.NullDisplayText = " ";
            });

            settings.Columns.Add(column =>
            {
                column.FieldName = "Remark";
                column.Caption = "Comment";
            });

            settings.CustomUnboundColumnData = (sender, e) =>
            {
                if (e.Column.FieldName == "Total")
                {
                    double time = Convert.ToDouble(e.GetListSourceFieldValue("Hours"));
                    if (time == 0) { e.Value = null; }
                    else
                    {
                        e.Value = DateTime.MinValue.Add(TimeSpan.FromSeconds(time));
                    }
                }
            };

            //Summary
            settings.Settings.ShowFooter = true;


            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "Hours");

            settings.SummaryDisplayText = (sender, e) =>
            {
                if (e.IsGroupSummary)
                {
                    if (e.Item.FieldName == "Hours")
                    {
                        double time = Double.Parse(e.Value.ToString());
                        string hours = Math.Floor((TimeSpan.FromSeconds(time)).TotalHours) > 9 ? Math.Floor((TimeSpan.FromSeconds(time)).TotalHours).ToString() : "0" + Math.Floor((TimeSpan.FromSeconds(time)).TotalHours).ToString();
                        string minutes = (TimeSpan.FromSeconds(time)).Minutes > 9 ? (TimeSpan.FromSeconds(time)).Minutes.ToString() : "0" + (TimeSpan.FromSeconds(time)).Minutes;
                        e.Text = "Total hours = " + hours + ":" + minutes;
                    }
                }
            };
            return settings;
        }
        #endregion
        #region Export

        [ValidateInput(false)]
        public void ExportToParams(int Reporttype, int ReportFormat, List<int> a)
        {
            Session["Users"] = a;
            Session["Reporttype"] = Reporttype;
            Session["ReportFormat"] = ReportFormat;
        }

        //[ValidateInput(false)]
        public ActionResult ExportTo()
        {

            int department = Convert.ToInt32(Session["department"]);
            int company = Convert.ToInt32(Session["company"]);
            int building_id = Convert.ToInt32(Session["Buidlidngid"]);
            string StartDate = (string)Session["TAStartDate"];
            string StoptDate = (string)Session["TAStoptDate"];
            List<int> UserIds = (List<int>)Session["Users"];
            int? Reporttype = (int?)Session["Reporttype"];
            int? ReportFormat = (int?)Session["ReportFormat"];
            int? timeFormat = (int?)Session["timeFormat"];


            DateTime From = DateTime.ParseExact(StartDate, "dd.MM.yyyy",
                          System.Globalization.CultureInfo.InvariantCulture);
            DateTime To = DateTime.ParseExact(StoptDate, "dd.MM.yyyy",
                                           System.Globalization.CultureInfo.InvariantCulture).AddDays(1);

            if (Session["Language"] == null)
            {
                countryflag = "EN";
            }
            else
            {
                countryflag = Session["Language"].ToString();
            }

            if (Reporttype == 1)
            { // GridView
                var tamvm = CreateViewModel<TAMsUserViewModel>();
                tamvm.TAMsUserItems = GetUsersTAMoves(From, To, UserIds);
                if (ReportFormat == 1)
                { // pdf
                    return GridViewExtension.ExportToPdf(GetGridSettings(timeFormat), tamvm.TAMsUserItems, GetPdfExportOptions());
                }
                else
                {
                    TaReportViewNewCustomoseUser tavm = new TaReportViewNewCustomoseUser();
                    List<TaNewUserDetails> listta_user = new List<TaNewUserDetails>();
                    int i = 0;


                    DateTime f1 = Convert.ToDateTime(Session["datefrom"]);
                    DateTime f2 = Convert.ToDateTime(Session["dateto"]);
                    int buildingid = Convert.ToInt32(Session["buildingid"]);

                    List<itembuilding> Building_Data = new List<itembuilding>();
                    var ctvm = new BuildingNameViewModel();
                    //string getdayname = "";
                    //var result = new List<dynamic>();
                    //dynamic result = new ExpandoObject();

                    // List<TaNewUserDetails> result1 = new List<TaNewUserDetails>();

                    // List<object> result = new List<object>();
                    if (building_id == 0)
                    {
                        var result1 = (from Ta in db.NewTaMoves
                                       join U in db.User on Ta.UserId equals U.Id
                                       join C in db.Companies on U.CompanyId equals C.Id
                                       join os in db.Title on U.TitleId equals os.Id into t
                                       from rt in t.DefaultIfEmpty()

                                       where Ta.Started >= From && Ta.Finished <= To
                                       && UserIds.Contains(Ta.UserId) && Ta.IsDeleted == false

                                       select new
                                       {
                                           companyname = C.Name,
                                           C.Comment,
                                           Ta.UserId,
                                           U.FirstName,
                                           U.LastName,
                                           U.CompanyId,
                                           U.PersonalCode,
                                           rt.Name,
                                           U.Id,
                                           Ta.Started,
                                           Ta.Finished
                                       }).OrderBy(x => x.Id).ToList();
                        if (company == 0)
                        {

                            result1 = result1.OrderBy(x => x.CompanyId).ToList();

                        }
                        int temp = 0;
                        int temp1 = 0;


                        var comps1 = result1.Select(x => x.CompanyId).Distinct();

                        foreach (var items in result1)
                        {
                            DayOfWeek dow = items.Started.DayOfWeek;


                            //if (countryflag == "EN")
                            //{
                            //    //getdayname = dow.ToString() + " ";
                            //}
                            //else
                            //{


                            //}
                            var compcount = result1.Where(x => x.CompanyId == items.CompanyId).Count();

                            DateTime date = items.Started;
                            string onlydate = date.ToString("dd/mm/yyyy");
                            DateTime st = items.Started;

                            string checkin1 = st.ToString("HH:mm");

                            DateTime fn = items.Finished;
                            string checkout1 = fn.ToString("HH:mm");

                            TimeSpan checkin = date.TimeOfDay;
                            TimeSpan checkout = fn.TimeOfDay;
                            var tottaltime = checkout.Subtract(checkin);
                            string hh = tottaltime.Hours < 10 ? "0" + tottaltime.Hours : tottaltime.Hours.ToString();
                            string mm = tottaltime.Minutes < 10 ? "0" + tottaltime.Minutes : tottaltime.Minutes.ToString();
                            string rr = hh + ":" + mm;

                            var timeDiff = tottaltime.TotalSeconds;
                            double sec = timeDiff;


                            i = i + 1;
                            int k = 0; k++; int p = 0; p++;
                            Session["FullNmae"] = items.FirstName + " " + items.LastName;
                            if (temp != items.UserId)
                            {
                                temp = items.UserId;
                                k = 0;
                            }

                            //string colflag = "";

                            //if (k == 0)
                            //{
                            //    colflag = "No";
                            //}
                            //else
                            //{
                            //    colflag = "No";
                            //}
                            if (temp1 != items.CompanyId)
                            {
                                temp1 = Convert.ToInt32(items.CompanyId);
                                p = 0;
                            }
                            //string companycolor = "";
                            //if (p == 0)
                            //{
                            //    companycolor = "Yes";
                            //}
                            //else
                            //{
                            //    companycolor = "No";
                            //}
                            string companydeatils1 = items.companyname + "  " + items.Comment;
                            listta_user.Add(new TaNewUserDetails()
                            {
                                id = i,
                                UserId = items.UserId,
                                CompnyId = Convert.ToInt32(items.CompanyId),
                                FirstName = items.FirstName,
                                LastName = items.LastName,
                                companydeatils = companydeatils1,
                                PersonalCode = items.PersonalCode,
                                Name = items.Name,
                                Started = items.Started,
                                checkin = checkin1,
                                checkout = checkout1,
                                Hours = sec,
                                totaltime = rr,
                                FullName = items.FirstName + " " + items.LastName
                                //colorflag = colflag,
                                //companycolrflag = companycolor

                            });
                        }
                        //ViewBag.Result = result1;
                        tavm.TaUserDetails = listta_user;
                        //XlsExportOptionsEx op = new XlsExportOptionsEx();
                        //op.CustomizeCell += Op_CustomizeCell;
                        XlsExportOptionsEx exportOptions = new XlsExportOptionsEx();
                        exportOptions.CustomizeSheetHeader += options_CustomizeSheetHeader;
                        CustomSummaryEventArgs custsumm = new CustomSummaryEventArgs();

                        exportOptions.CustomizeCell += new DevExpress.Export.CustomizeCellEventHandler(exportOptions_CustomizeCell);


                        if (countryflag == "EN")
                        {
                            return GridViewExtension.ExportToXls(GetPivotSettingsNewBuildingForEnglish(timeFormat), tavm.TaUserDetails, exportOptions);

                        }
                        else
                        {
                            return GridViewExtension.ExportToXls(GetPivotSettingsNewBuilding(timeFormat), tavm.TaUserDetails, exportOptions);
                        }
                    }
                    else
                    {
                        if (building_id != 0)
                        {


                            var result2 = (from Ta in db.NewTaMoves
                                           join U in db.User on Ta.UserId equals U.Id
                                           join C in db.Companies on U.CompanyId equals C.Id
                                           join b in db.BuildingObject on Ta.FinishedBoId equals b.Id
                                           join os in db.Title on U.TitleId equals os.Id into t
                                           from rt in t.DefaultIfEmpty()

                                           where Ta.Started >= From && Ta.Finished <= To
                                           && UserIds.Contains(Ta.UserId) && Ta.IsDeleted == false && b.BuildingId == building_id

                                           select new
                                           {
                                               companyname = C.Name,
                                               C.Comment,
                                               Ta.UserId,
                                               U.FirstName,
                                               U.LastName,
                                               U.CompanyId,
                                               U.PersonalCode,
                                               rt.Name,
                                               U.Id,
                                               Ta.Started,
                                               Ta.Finished
                                           }).OrderBy(x => x.Id).ToList();
                            if (company == 0)
                            {
                                result2 = result2.OrderBy(x => x.CompanyId).ToList();
                            }

                            int temp2 = 0;
                            int temp3 = 0;

                            foreach (var items in result2)
                            {

                                var compcount = result2.Where(x => x.CompanyId == items.CompanyId).Count();

                                DateTime date = items.Started;
                                string onlydate = date.ToString("dd/mm/yyyy");
                                DateTime st = items.Started;
                                string checkin1 = st.ToString("HH:mm");

                                DateTime fn = items.Finished;
                                string checkout1 = fn.ToString("HH:mm");

                                TimeSpan checkin = date.TimeOfDay;
                                TimeSpan checkout = fn.TimeOfDay;
                                var tottaltime = checkout.Subtract(checkin);
                                string hh = tottaltime.Hours < 10 ? "0" + tottaltime.Hours : tottaltime.Hours.ToString();
                                string mm = tottaltime.Minutes < 10 ? "0" + tottaltime.Minutes : tottaltime.Minutes.ToString();
                                string rr = hh + ":" + mm;

                                var timeDiff = tottaltime.TotalSeconds;
                                double sec = timeDiff;


                                i = i + 1;
                                int k = 0; k++; int p = 0; p++; int n = 0; n++;
                                Session["FullNmae"] = items.FirstName + " " + items.LastName;
                                if (temp2 != items.UserId)
                                {
                                    temp2 = items.UserId;
                                    k = 0;

                                }

                                //string colflag = "";

                                //if (k == 0)
                                //{
                                //    colflag = "No";
                                //}
                                //else
                                //{
                                //    colflag = "No";
                                //}
                                if (temp3 != items.CompanyId)
                                {
                                    temp3 = Convert.ToInt32(items.CompanyId);
                                    p = 0;
                                }
                                //string companycolor = "";
                                //if (p == 0)
                                //{
                                //    companycolor = "Yes";
                                //}
                                //else
                                //{
                                //    companycolor = "No";
                                //}
                                string companydeatils1 = items.companyname + "  " + items.Comment;
                                listta_user.Add(new TaNewUserDetails()
                                {
                                    id = i,
                                    UserId = items.UserId,
                                    CompnyId = Convert.ToInt32(items.CompanyId),
                                    FirstName = items.FirstName,
                                    LastName = items.LastName,
                                    companydeatils = companydeatils1,
                                    PersonalCode = items.PersonalCode,
                                    Name = items.Name,
                                    //Started = getdayname + items.Started.ToString("dd/MM/yyyy"),
                                    Started = items.Started,
                                    checkin = checkin1,
                                    checkout = checkout1,
                                    Hours = sec,
                                    totaltime = rr,
                                    FullName = items.FirstName + " " + items.LastName
                                    //colorflag = colflag,
                                    //companycolrflag = companycolor

                                });
                            }

                            ViewBag.Result = result2;
                        }

                        tavm.TaUserDetails = listta_user;

                        XlsExportOptionsEx exportOptions = new XlsExportOptionsEx();
                        exportOptions.CustomizeSheetHeader += options_CustomizeSheetHeader;
                        CustomSummaryEventArgs custsumm = new CustomSummaryEventArgs();

                        exportOptions.CustomizeCell += new DevExpress.Export.CustomizeCellEventHandler(exportOptions_CustomizeCell);

                        if (Session["Language"] == null)
                        {
                            countryflag = "EN";
                        }
                        else
                        {
                            countryflag = Session["Language"].ToString();
                        }
                        if (countryflag == "EN")
                        {
                            return GridViewExtension.ExportToXls(GetPivotSettingsNewBuildingForEnglish(timeFormat), tavm.TaUserDetails, exportOptions);

                        }
                        else
                        {
                            return GridViewExtension.ExportToXls(GetPivotSettingsNewBuilding(timeFormat), tavm.TaUserDetails, exportOptions);
                        }

                    }
                }

            }


            else
            { //PivotGrid
                TaReportViewNewCustomoseUser tavm = new TaReportViewNewCustomoseUser();
                List<TaNewUserDetails> listta_user = new List<TaNewUserDetails>();
                var tamvm = CreateViewModel<TAReportMounthViewModel>();
                Mapper.Map(GetTAreportsUsers(From, To, UserIds), tamvm.TAReportMounthItems);
                if (ReportFormat == 1)
                { // pdf
                    return PivotGridExtension.ExportToPdf(GetPivotSettings(timeFormat), tamvm.TAReportMounthItems, GetPdfExportOptions());
                }
                else
                { // excel
                    return PivotGridExtension.ExportToXls(GetPivotSettings(timeFormat), tamvm.TAReportMounthItems);
                }
            }

        }


        void exportOptions_CustomizeCell(DevExpress.Export.CustomizeCellEventArgs ea)
        {
            GridViewExcelDataPrinter printer = (GridViewExcelDataPrinter)ea.DataSourceOwner;
            var index = printer.GetVisibleIndex(ea.RowHandle);
            string fullname = Convert.ToString(printer.Grid.GetRowValues(index, "colorflag"));
            string compnayname = Convert.ToString(printer.Grid.GetRowValues(index, "companycolrflag"));
            // string checlom = Convert.ToString(printer.Grid.GetRowValues(index, "checkin"));

            if (ea.AreaType == DevExpress.Export.SheetAreaType.Header)
            {
                ea.Formatting.Font.Bold = true;
                ea.Formatting.BackColor = System.Drawing.Color.LightGray;
                ea.Handled = true;
            }

            if (ea.AreaType == DevExpress.Export.SheetAreaType.GroupHeader)
            {
                ea.Formatting.Font.Bold = true;
                ea.Formatting.BackColor = System.Drawing.Color.LightGray;
                ea.Handled = true;
            }
            if (fullname == "Yes")
            {
                ea.Formatting.BackColor = System.Drawing.Color.LightGray;
                ea.Formatting.Font.Bold = true;
                ea.Handled = true;
            }
        }
        public ActionResult TABuildingListforChange()
        {
            DateTime f1 = Convert.ToDateTime(Session["datefrom"]);
            DateTime f2 = Convert.ToDateTime(Session["dateto"]);
            int buildingid = Convert.ToInt32(Session["buildingid"]);

            List<itembuilding> Building_Data = new List<itembuilding>();
            var ctvm = new BuildingNameViewModel();

            object result = "";
            if (buildingid == 0)
            {
                result = db.TABuildingName.Where(x => x.IsDeleted == false).OrderByDescending(x => x.Id).ToList();
            }
            else
                result = db.TABuildingName.Where(x => x.Id == buildingid && x.IsDeleted == false).OrderByDescending(x => x.Id).ToList();


            ViewBag.Data = result;
            foreach (var items in ViewBag.Data)
            {
                Building_Data.Add(new itembuilding() { Id = items.Id, Name = items.Name, BuildingId = items.BuildingId, Address = items.Address, BuildingLicense = items.BuildingLicense, CadastralNr = items.CadastralNr, ValidFrom = items.ValidFrom, ValidTo = items.ValidTo, Customer = items.Customer, Contractor = items.Contractor, Contract = items.Contract, Sum = items.Sum });


            }

            ctvm.BuildingData = Building_Data;
            ctvm.buildings = GetBuildingsForGrid();

            return PartialView("BindTaBuildName", ctvm);
        }
        public ActionResult TABuildingList(string datefrom, string dateto, int buildingid)
        {
            DateTime From = DateTime.ParseExact(datefrom, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);


            DateTime To = DateTime.ParseExact(dateto, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
            Session["datefrom"] = From;
            Session["dateto"] = To;
            Session["buildingid"] = buildingid;

            List<itembuilding> Building_Data = new List<itembuilding>();
            var ctvm = new BuildingNameViewModel();
            object result = "";
            if (buildingid == 0)
            {
                result = db.TABuildingName.Where(x => x.IsDeleted == false).OrderByDescending(x => x.Id).ToList();
            }
            else
                result = db.TABuildingName.Where(x => x.IsDeleted == false).OrderByDescending(x => x.Id).ToList();

            //var result = db.TABuildingName.ToList();

            ViewBag.MenuData = result;
            foreach (var items in ViewBag.MenuData)
            {
                Building_Data.Add(new itembuilding() { Id = items.Id, Name = items.Name, BuildingId = items.BuildingId, Address = items.Address, BuildingLicense = items.BuildingLicense, CadastralNr = items.CadastralNr, ValidFrom = items.ValidFrom, ValidTo = items.ValidTo, Customer = items.Customer, Contractor = items.Contractor, Contract = items.Contract, Sum = items.Sum });


            }

            ctvm.BuildingData = Building_Data;
            ctvm.buildings = GetBuildingsForGrid();
            ctvm.buildings = (ctvm.buildings).Where(x => x.IsDeleted == false);
            return PartialView("BindTaBuildName", ctvm);

        }

        public IEnumerable<itembuilding> GetBuildingsForGrid()
        {
            List<itembuilding> lib = new List<itembuilding>();

            var query = from b in db.Buildings
                        select new
                        {
                            buildingID = b.Id,
                            building = b.Name
                        };
            //query = query.Where(x => x.IsDeleted == 0);
            itembuilding ib_0 = new ViewModels.itembuilding();
            ib_0.BuildingId = 0;
            ib_0.BuildingName = "--Select--";
            lib.Add(ib_0);
            foreach (var q in query)
            {
                itembuilding ib = new ViewModels.itembuilding();
                ib.BuildingId = q.buildingID;
                ib.BuildingName = q.building;
                lib.Add(ib);
            }
            return lib;
        }

        [ValidateInput(false)]
        public ActionResult TAReportExportCallBack()
        {
            try

            {
                string FromDateTA = Session["TAStartDate"].ToString();
                string ToDateTA = Session["TAStoptDate"].ToString();
                DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
                                           System.Globalization.CultureInfo.InvariantCulture);


                DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",
                                           System.Globalization.CultureInfo.InvariantCulture).AddDays(1);

                List<User> usrs1 = new List<User>();
                int department = Convert.ToInt32(Session["department"]);
                int company = Convert.ToInt32(Session["company"]);
                List<User> usrs = new List<User>();
                List<int> usr = new List<int>();
                int BuildingId = Convert.ToInt32(Session["Buidlidngid"]);
                string BName = Session["BuildingName"].ToString();
                var ULVM = CreateViewModel<UserListViewModel>();
                if (CurrentUser.Get().IsSuperAdmin || CurrentUser.Get().IsDepartmentManager || CurrentUser.Get().IsCompanyManager)
                {
                    if (company == 0 && department == 0)
                    {
                        usr = _userRepository.FindAll(x => !x.IsDeleted).Select(x => x.Id).ToList();
                    }
                    else
                    {
                        if (department != 0)
                        {
                            usr = _UserDepartmentRepository.FindAll(x => !x.IsDeleted && x.DepartmentId == department).Select(x => x.UserId).ToList();
                        }
                        else
                        {
                            usr = _userRepository.FindAll(x => !x.IsDeleted && x.CompanyId == company).Select(x => x.Id).ToList();

                        }

                    }
                }
                else
                {
                    if (CurrentUser.Get().IsCompanyManager)
                    {
                        if (department == 0)
                        {//!!
                            usr = _userRepository.FindAll(x => !x.IsDeleted && x.CompanyId == company).Select(x => x.Id).ToList();
                        }
                        else
                        {
                            usr = _UserDepartmentRepository.FindAll(x => !x.IsDeleted && x.DepartmentId == department).Select(x => x.UserId).ToList();
                        }
                    }
                    else // if (CurrentUser.Get().IsDepartmentManager)
                    {
                        List<int> dep = _UserDepartmentRepository.FindByUserId(CurrentUser.Get().Id).Select(x => x.DepartmentId).ToList();
                        usr = _UserDepartmentRepository.FindAll(x => !x.IsDeleted && dep.Contains(x.DepartmentId)).Select(x => x.UserId).ToList();
                    }
                }

                usrs = _userRepository.FindAll(x => usr.Contains(x.Id) && !x.IsDeleted).ToList();


                if (BuildingId > 0)
                {



                    foreach (var u in usrs)
                    {

                        if (BuildingId != 0)
                        {
                            // var result = (from ta in db.NewTaMoves join B in db.BuildingObject on ta.FinishedBoId equals B.Id where B.BuildingId == BuildingId && ta.UserId == u.Id select new { ta.UserId, B.BuildingId }).OrderBy(ta => ta.UserId).FirstOrDefault();

                            string query1 = @"select distinct ta.Userid,b.BuildingId from TAMoves ta join BuildingObjects b on ta.FinishedBoId = b.Id where b.BuildingId = {0} and ta.UserId={1} and ta.IsDeleted={2} and ta.Started >={3} and ta.Finished<={4}  order by ta.UserId";

                            var buildingDetails = db.Database.SqlQuery<TaReport1>(query1, BuildingId, u.Id, false, From, To).FirstOrDefault();

                            var getbuildingname = db.TABuildingName.Where(x => x.IsDeleted == false && x.BuildingId == BuildingId).FirstOrDefault();


                            if (buildingDetails != null)
                            {

                                if (buildingDetails.BuildingId == BuildingId)
                                {
                                    if (getbuildingname != null)
                                    {
                                        u.buildingID = getbuildingname.BuildingId;
                                        u.buildingName = getbuildingname.Name;
                                    }
                                    usrs1.Add(u);
                                }
                            }
                        }

                    }
                }



                if (BuildingId != 0)
                {

                    Mapper.Map(usrs1, ULVM.Users);
                }
                else
                {
                    Mapper.Map(usrs, ULVM.Users);
                }

                return PartialView("TAReportExportUsers", ULVM.Users);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public ActionResult TAReportExport(int? department, int? company, string FromDateTA, string ToDateTA, int format, int? BuildingId, string BName)
        {

            DateTime From = DateTime.ParseExact(FromDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);


            DateTime To = DateTime.ParseExact(ToDateTA, "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture).AddDays(1);

            List<User> usrs = new List<User>();
            List<User> usrs1 = new List<User>();
            List<int> usr = new List<int>();
            var ULVM = CreateViewModel<UserListViewModel>();
            Session["TAStartDate"] = FromDateTA;
            Session["TAStoptDate"] = ToDateTA;
            Session["timeFormat"] = format;
            Session["department"] = department;
            Session["company"] = company;
            Session["BuildingName"] = BName;
            Session["Buidlidngid"] = BuildingId;
            if (CurrentUser.Get().IsSuperAdmin || CurrentUser.Get().IsDepartmentManager || CurrentUser.Get().IsCompanyManager)
            {
                if (CurrentUser.Get().IsSuperAdmin)
                {
                    if (company == null && department == null)
                    {
                        usr = _userRepository.FindAll(x => !x.IsDeleted).Select(x => x.Id).ToList();
                    }
                    else
                    {
                        if (department != null)
                        {
                            usr = _UserDepartmentRepository.FindAll(x => !x.IsDeleted && x.DepartmentId == department).Select(x => x.UserId).ToList();
                        }
                        else
                        {
                            usr = _userRepository.FindAll(x => !x.IsDeleted && x.CompanyId == company).Select(x => x.Id).ToList();

                        }
                    }
                }
                else
                {
                    if (CurrentUser.Get().IsCompanyManager)
                    {
                        if (department == null)
                        {//!!
                            usr = _userRepository.FindAll(x => !x.IsDeleted && x.CompanyId == company).Select(x => x.Id).ToList();
                        }
                        else
                        {
                            usr = _UserDepartmentRepository.FindAll(x => !x.IsDeleted && x.DepartmentId == department).Select(x => x.UserId).ToList();
                        }
                    }
                    else // if (CurrentUser.Get().IsDepartmentManager)
                    {
                        List<int> dep = _UserDepartmentRepository.FindByUserId(CurrentUser.Get().Id).Select(x => x.DepartmentId).ToList();
                        usr = _UserDepartmentRepository.FindAll(x => !x.IsDeleted && dep.Contains(x.DepartmentId)).Select(x => x.UserId).ToList();
                    }
                }
            }
            usrs = _userRepository.FindAll(x => usr.Contains(x.Id) && !x.IsDeleted).ToList();

            if (BuildingId > 0)
            {



                foreach (var u in usrs)
                {

                    if (BuildingId != 0 && BuildingId != null)
                    {

                        //string query1 = @"select distinct ta.Userid,b.BuildingId from TAMoves ta join BuildingObjects b on ta.FinishedBoId = b.Id where b.BuildingId = {0} and ta.UserId={1} and ta.Started={2} and ta.Finished={3} order by ta.UserId";

                        //var buildingDetails = db.Database.SqlQuery<TaReport1>(query1, BuildingId, u.Id,From,To).FirstOrDefault();
                        string query1 = @"select distinct ta.Userid,b.BuildingId from TAMoves ta join BuildingObjects b on ta.FinishedBoId = b.Id where b.BuildingId = {0} and ta.UserId={1} and ta.IsDeleted={2} and ta.Started >={3} and ta.Finished<={4}  order by ta.UserId";

                        var buildingDetails = db.Database.SqlQuery<TaReport1>(query1, BuildingId, u.Id, false, From, To).FirstOrDefault();


                        // Getting BuildingName from TabuildinName
                        var getbuildingname = db.TABuildingName.Where(x => x.IsDeleted == false && x.BuildingId == BuildingId).FirstOrDefault();

                        //var getbuildingname = db.Buildings.Where(x => x.Id == BuildingId).FirstOrDefault();



                        if (buildingDetails != null)
                        {

                            if (buildingDetails.BuildingId == BuildingId)
                            {
                                if (getbuildingname != null)
                                {
                                    u.buildingID = getbuildingname.BuildingId;
                                    u.buildingName = getbuildingname.Name;
                                }
                                usrs1.Add(u);
                            }
                        }

                    }

                }

            }

            if (BuildingId > 0)
            {
                try
                {
                    Mapper.Map(usrs1, ULVM.Users);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                Mapper.Map(usrs, ULVM.Users);
            }

            Session["TypedListModel"] = ULVM;

            return PartialView("TAReportExport", Session["TypedListModel"]);
        }


        #endregion
        #region other

        /*
        [ValidateInput(false)]
        public ActionResult MounthBatchGridEditA([ModelBinder(typeof(DevExpressEditorsBinder))]MVCxGridViewBatchUpdateValues<TAReport, int> updateValues)
        {
            foreach (var product in updateValues.Insert)
            {
                if (updateValues.IsValid(product))
                    InsertProduct(product, updateValues);
            }
            foreach (var product in updateValues.Update)
            {
                if (updateValues.IsValid(product))
                    UpdateProduct(product, updateValues);
            }
            foreach (var productID in updateValues.DeleteKeys)
            {
                DeleteProduct(productID, updateValues);
            }
            string StartDate = (string)Session["TAStartDate"];
            string StoptDate = (string)Session["TAStoptDate"];
            int? CompanyId = (int?)Session["TACompanyId"];
            int? department = (int?)Session["TAdepartmentId"];
            DateTime From = DateTime.ParseExact(StartDate, "dd.mm.yyyy",
                           System.Globalization.CultureInfo.InvariantCulture);


            DateTime To = DateTime.ParseExact(StoptDate, "dd.mm.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

            return PartialView("TAMounthReportGridViewPartialView", GetTAreports(From, To, CompanyId, department));
        }
 */
        /*
                        [HttpPost]
                        public ActionResult BatchEditing(GridViewBatchEditMode editMode, GridViewBatchStartEditAction startEditAction)
                        {
                            GridViewEditingDemosHelper.BatchEditMode = editMode;
                            GridViewEditingDemosHelper.BatchStartEditAction = startEditAction;
                            return DemoView("BatchEditing", NorthwindDataProvider.GetEditableProducts());
                        }*/

        protected void InsertProduct(TAMove product, MVCxGridViewBatchUpdateValues<TAMove, int> updateValues)
        {
            // try {
            //   NorthwindDataProvider.InsertProduct(product);
            // }
            //catch(Exception e) {
            //    updateValues.SetErrorText(product, e.Message);
            //}
        }
        protected void UpdateProduct(TAMove product, MVCxGridViewBatchUpdateValues<TAMove, int> updateValues)
        {
            try
            {
                //NorthwindDataProvider.UpdateProduct(product);
            }
            catch (Exception e)
            {
                updateValues.SetErrorText(product, e.Message);
            }
        }
        protected void DeleteProduct(int productID, MVCxGridViewBatchUpdateValues<TAMove, int> updateValues)
        {
            //  try {
            //   NorthwindDataProvider.DeleteProduct(productID);
            // }
            //catch(Exception e) {
            //    updateValues.SetErrorText(productID, e.Message);
            //}
        }
        #endregion
        #region GetMethods
        private IEnumerable<BuildingObject> GetBuildingObjects(int? companyId)
        {

            IEnumerable<BuildingObject> BuildingObjects = new List<BuildingObject>();
            BuildingObjects = _BuildingObjectRepository.FindAll(x => x.TypeId == 8 && !x.IsDeleted);

            return BuildingObjects;
        }
        public IEnumerable<itembuilding> GetTANames(int? bid)
        {
            List<itembuilding> Building_Data = new List<itembuilding>();

            var result = db.TABuildingName.Where(x => x.IsDeleted == false).ToList();

            ViewBag.MenuData = result;
            foreach (var items in ViewBag.MenuData)
            {
                Building_Data.Add(new itembuilding() { Id = items.Id, Name = items.Name, BuildingId = items.BuildingId, Address = items.Address, BuildingLicense = items.BuildingLicense, CadastralNr = items.CadastralNr, ValidFrom = items.ValidFrom.ToString("dd/M/yyyy"), ValidTo = items.ValidTo.ToString("dd/M/yyyy") });


            }


            return Building_Data;


        }

        private IEnumerable<TAMove> GetUserTAMoves(int userId, DateTime From, DateTime To)
        {
            IEnumerable<TAMove> UserTAMoves = new List<TAMove>();
            UserTAMoves = _TAMoveRepository.FindAll(x => x.UserId == userId && !x.IsDeleted && x.Started >= From && x.Finished <= To);
            return UserTAMoves;
        }
        private IEnumerable<TAMove> GetUsersTAMoves(DateTime From, DateTime To, int? company, int? department, int? BuildingId)
        {
            IEnumerable<TAMove> UserTAMoves = new List<TAMove>();
            // IEnumerable<TAMove> UserTAMoves1 = new List<TAMove>();
            List<TAMove> UserTAMoves1 = new List<TAMove>();
            if (company != null)
            {
                List<int> usr = _userRepository.FindAll(x => x.Active == true && x.CompanyId == company.GetValueOrDefault() && x.IsDeleted == false).Select(x => x.Id).ToList();
                if (department != null)
                {
                    List<int> dps = _UserDepartmentRepository.FindAll(x => !x.IsDeleted && x.DepartmentId == department).Select(x => x.UserId).ToList();
                    usr = usr.FindAll(x => dps.Contains(x)).ToList();//               dps.Contains(usr);
                }
                UserTAMoves = _TAMoveRepository.FindAll(x => !x.IsDeleted && x.Started >= From && x.Finished <= To.AddDays(1) && usr.Contains(x.UserId));
            }
            else
            {
                UserTAMoves = _TAMoveRepository.FindAll(x => !x.IsDeleted && x.Started >= From && x.Finished <= To.AddDays(1));
            }
            if (BuildingId != null && BuildingId != 0)
            {
                foreach (var u in UserTAMoves)
                {
                    //var tr = _userRepository.FindAll(x => x.Id == u.UserId && x.buildingID == BuildingId).SingleOrDefault();

                    var tr = db.TAreport.Where(x => x.UserId == u.UserId && x.BuildingId == BuildingId && x.IsDeleted == false).Any();
                    if (tr)
                    {
                        UserTAMoves1.Add(u);
                    }
                }
                if (UserTAMoves1 != null)
                {
                    UserTAMoves = UserTAMoves1;
                }
            }

            return UserTAMoves;
        }
        private IEnumerable<TAReport> GetTAreports(DateTime From, DateTime To, int usr)
        {
            IEnumerable<TAReport> retvalue = new List<TAReport>();
            retvalue = _reportRepository.FindAll(x => !x.IsDeleted && x.ReportDate >= From && x.ReportDate <= To && x.UserId == usr);

            return retvalue;
        }
        private IEnumerable<TAReport> GetTAreports(DateTime From, DateTime To, int? SelectedCompanyId, int? department)
        {
            IEnumerable<TAReport> retvalue = new List<TAReport>();
            if (SelectedCompanyId != null)
            {
                List<int> usr = _userRepository.FindAll(x => x.Active == true && x.CompanyId == SelectedCompanyId.GetValueOrDefault() && x.IsDeleted == false).Select(x => x.Id).ToList();
                if (department != null)
                {
                    List<int> dps = _UserDepartmentRepository.FindAll(x => !x.IsDeleted && x.DepartmentId == department).Select(x => x.UserId).ToList();
                    //  List<int> dps = _UserDepartmentRepository.FindAll(x => x.DepartmentId == department).Select(x => x.Id).ToList();
                    usr = usr.FindAll(x => dps.Contains(x)).ToList();//               dps.Contains(usr);
                }
                retvalue = _reportRepository.FindAll(x => !x.IsDeleted && x.ReportDate >= From && x.ReportDate <= To && usr.Contains(x.UserId));
                //UserTAMoves = _TAMoveRepository.FindAll(x => !x.IsDeleted && x.Started >= From && x.Finished <= To && usr.Contains(x.UserId));
            }
            else
            {
                if (department != null)
                {
                    retvalue = _reportRepository.FindAll(x => !x.IsDeleted && x.ReportDate >= From && x.ReportDate <= To.AddDays(1) && x.DepartmentId == department);
                }
                else
                {
                    retvalue = _reportRepository.FindAll(x => !x.IsDeleted && x.ReportDate >= From && x.ReportDate <= To.AddDays(1));
                    //UserTAMoves = _TAMoveRepository.FindAll(x => !x.IsDeleted && x.Started >= From && x.Finished <= To);
                }
            }

            return retvalue;
        }
        private IEnumerable<TAReport> GetTAreportsUsers(DateTime From, DateTime To, List<int> UserIds)
        {
            IEnumerable<TAReport> retvalue = new List<TAReport>();
            if (UserIds != null)
            {
                retvalue = _reportRepository.FindAll(x => !x.IsDeleted && UserIds.Contains(x.UserId) && x.ReportDate >= From && x.ReportDate <= To);
            }
            return retvalue;
        }
        private IEnumerable<TAMove> GetUserTAMoves(string StartDate1, string StoptDate1, int UserId)
        {
            IEnumerable<TAMove> retvalue = new List<TAMove>();
            if (StartDate1 != null && StoptDate1 != null)
            {

                retvalue = _TAMoveRepository.FindAll(x => x.UserId == UserId && x.IsDeleted == false);
            }

            return retvalue;
        }
        private IEnumerable<TAMove> GetUsersTAMoves(DateTime From, DateTime To, List<int> UserIds)
        {
            IEnumerable<TAMove> retvalue = new List<TAMove>();
            if (UserIds != null)
            {
                retvalue = _TAMoveRepository.FindAll(x => !x.IsDeleted && x.Started >= From && x.Finished <= To && UserIds.Contains(x.UserId));
            }
            return retvalue;
        }
        #endregion
        #region Class
        //TAMounthReportGridViewPartialView.ascx
        public class PivotGridFeaturesDemosHelper
        {

            static PivotGridSettings drillDownPivotGridSettings;
            public static PivotGridSettings DrillDownPivotGridSettings
            {
                get
                {
                    if (drillDownPivotGridSettings == null)
                        drillDownPivotGridSettings = CreateDrillDownPivotGridSettings();
                    return drillDownPivotGridSettings;
                }
            }
            static PivotGridSettings CreateDrillDownPivotGridSettings()
            {
                var settings = new PivotGridSettings();
                settings.Name = "pivotGrid";
                settings.CallbackRouteValues = new { Controller = "TAReport", Action = "TAMounthReportGridViewPartialA" };
                //settings.Width = Unit.Percentage(100);

                settings.Fields.Add(field =>
                {
                    field.Area = PivotArea.RowArea;
                    field.Caption = "Last Name";
                    field.FieldName = "UserName";

                });


                settings.Fields.Add(field =>
                {
                    field.Area = PivotArea.ColumnArea;

                    field.FieldName = "ReportDate";
                    field.Caption = "ReportDate";
                    field.GroupInterval = PivotGroupInterval.DateMonthYear;//.DateMonth;

                });

                settings.Fields.Add(field =>
                {
                    field.Area = PivotArea.ColumnArea;
                    field.FieldName = "ReportDate";
                    field.Caption = "Quarter";
                    field.GroupInterval = PivotGroupInterval.DateDay;

                });

                settings.Fields.Add(field =>
                {
                    field.Area = PivotArea.DataArea;
                    field.FieldName = "Hours";  //ViewResources.SharedStrings.TAHours;
                });
                return settings;
            }
        }
        #endregion

        #region
        public ActionResult Bands()
        {
            return PartialView("Bands");
        }
        public ActionResult DailyWorkingReport()
        {
            return PartialView("DailyWorkingReport");
        }

        public ActionResult ExportTo1()
        {
            TaReportViewNewCustomoseUser tavm = new TaReportViewNewCustomoseUser();
            List<TaNewUserDetails> listta_user = new List<TaNewUserDetails>();
            XlsExportOptionsEx exportOptions = new XlsExportOptionsEx();
            exportOptions.CustomizeSheetHeader += options_CustomizeSheetHeader;
            CustomSummaryEventArgs custsumm = new CustomSummaryEventArgs();
            exportOptions.ExportType = ExportType.WYSIWYG;
            exportOptions.CustomizeCell += new DevExpress.Export.CustomizeCellEventHandler(exportOptions_CustomizeCell);

            tavm.TaUserDetails = listta_user;
            return GridViewExtension.ExportToXls(GetTaxReport(), tavm.TaUserDetails, exportOptions);

        }
        private GridViewSettings GetTaxReport()
        {
            var settings1 = new GridViewSettings();
            settings1.Name = "gvmain";

            // Export-specific settings  
            // settings.SettingsExport.ExportedRowType = DevExpress.Web.Export.GridViewExportedRowType.All;
            settings1.SettingsExport.FileName = "Report.xls";
            settings1.SettingsExport.PaperKind = System.Drawing.Printing.PaperKind.A4;
            settings1.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            settings1.ControlStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            settings1.KeyFieldName = "id";
            settings1.Settings.ShowFilterRow = true;
            settings1.SettingsExport.Styles.Header.Font.Bold = true;
            settings1.SettingsText.EmptyDataRow = " ";

            settings1.Columns.AddBand(cc1 =>
            {
                var settings = new GridViewSettings();
                settings.Name = "gvmain1";
                settings.Settings.ShowGroupPanel = true;
                settings.SettingsText.GroupPanel = @"Informācija no Elektroniskās darba laika uzskaites sistēmas (EDLUS) par būvobjektā nodarbinātajām personām un nostrādātajām stundām par periodu _________________________(taksācijas gads, mēnesis)";
                settings.Styles.GroupPanel.Font.Bold = true;
                settings.Styles.GroupPanel.HorizontalAlign = HorizontalAlign.Center;
                settings.Styles.GroupPanel.Wrap = DefaultBoolean.True;
                settings.Columns.AddBand(cons =>
                {
                    cons.Columns.AddBand(constructor =>
                    {
                        constructor.Caption = "Informācija par galvenā būvdarbu veicēja noslēgto<br> būvdarbu līgumu ar būvniecības ierosinātāju";
                        constructor.Columns.Add(c =>
                        {
                            c.FieldName = "Būvniecības ierosinātāja<br> nosaukums";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;
                            c.Width = Unit.Pixel(60);
                            c.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                        });
                        constructor.Columns.AddBand(c =>
                        {
                            c.Caption = "Būvniecības ierosinātāja<br> nodokļu maksātāja<br> reģistrācijas kods";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;
                            c.Width = Unit.Pixel(60);
                            c.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                        });
                        constructor.Columns.Add(c1 =>
                        {
                            c1.FieldName = "Līguma datums";
                            c1.HeaderStyle.Wrap = DefaultBoolean.True;
                            c1.Width = Unit.Pixel(60);
                            c1.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                        });
                        constructor.Columns.Add(c1 =>
                        {
                            c1.FieldName = "Līguma summa bez PVN";
                            c1.HeaderStyle.Wrap = DefaultBoolean.True;
                            c1.Width = Unit.Pixel(60);
                            c1.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                        });
                        constructor.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                        constructor.HeaderStyle.Wrap = DefaultBoolean.True;
                        constructor.Width = Unit.Pixel(240);
                    });

                    cons.Columns.AddBand(constructor => { constructor.HeaderStyle.CssClass = "myBorder"; });
                    cons.Columns.AddBand(constructor => { constructor.HeaderStyle.CssClass = "myBorder"; });
                    cons.Columns.AddBand(constructor => { constructor.HeaderStyle.CssClass = "myBorder"; });
                    cons.Columns.AddBand(constructor => { constructor.HeaderStyle.CssClass = "myBorder"; });
                    cons.Columns.AddBand(constructor => { constructor.HeaderStyle.CssClass = "myBorder"; });
                    cons.HeaderStyle.BackColor = System.Drawing.Color.AliceBlue;
                    cons.HeaderStyle.ForeColor = System.Drawing.Color.White;
                });

            });

            settings1.Columns.AddBand(cc1 =>
            {
                var settings = new GridViewSettings();

                settings.Name = "gvBands";
                //settings.CallbackRouteValues = new { Controller = "Columns", Action = "Bands" };

                settings.Settings.ShowGroupPanel = false;

                settings.Columns.AddBand(c =>
                {
                    c.Caption = "Nr.p.k.";
                    //c.FieldName = "Nr.p.k.";
                    c.HeaderStyle.Wrap = DefaultBoolean.True;
                });
                settings.Columns.AddBand(c =>
                {
                    c.Caption = "Informācijas iesniegšanas datums VID";
                    c.HeaderStyle.Wrap = DefaultBoolean.True;
                });

                settings.Columns.AddBand(c =>
                {
                    c.Caption = "Taksācijas gads";
                    c.HeaderStyle.Wrap = DefaultBoolean.True;
                });

                settings.Columns.AddBand(c =>
                {
                    c.Caption = "Taksācijas mēnesis";
                    c.HeaderStyle.Wrap = DefaultBoolean.True;
                });

                settings.Columns.AddBand(info =>
                {
                    info.Caption = "Informācija par būvlaukumā nodarbinātu personu";

                    info.Columns.AddBand(empdet =>
                    {
                        empdet.Caption = "Nodarbinātā";
                        empdet.Columns.Add("Vārds");
                        empdet.Columns.Add("Uzvārds");
                        empdet.Columns.Add(c =>
                        {
                            c.FieldName = "Personas kods";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;
                        });
                        empdet.Columns.AddBand(c =>
                        {
                            c.Caption = "Ja personai nav personas koda";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;

                            c.Columns.Add(c1 =>
                            {
                                c1.FieldName = "Dzimšanas datums, mēnesis, gads, vai vīzas vai uzturēšanās atļaujas numurs";
                                c1.HeaderStyle.Wrap = DefaultBoolean.True;
                            });
                        });
                        empdet.Columns.Add("Amats");
                        empdet.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    });

                    info.Columns.AddBand(initialdates =>
                    {
                        initialdates.Caption = "Darba devēja";
                        initialdates.Columns.Add(c =>
                        {
                            c.FieldName = "Amats";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;
                        });
                        initialdates.Columns.Add(c =>
                        {
                            c.FieldName = "Nosaukums";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;
                        });
                        initialdates.Columns.Add(c =>
                        {
                            c.FieldName = "Vārds, uzvārds, ja darba devējs ir fiziskā persona";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;
                        });
                        initialdates.Columns.Add(c =>
                        {
                            c.FieldName = "Reģistrācijas kods vai personas kods";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;
                        });
                        initialdates.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    });

                    info.Columns.AddBand(c1 =>
                    {
                        c1.Caption = "Būvatļaujas numurs vai nekustamā īpašuma objekta kadastra apzīmējums";
                        c1.HeaderStyle.Wrap = DefaultBoolean.True;
                    });
                    info.Columns.AddBand(c2 =>
                    {
                        c2.Caption = "Faktiski nostrādātais darba laiks (stundās)";
                        c2.HeaderStyle.Wrap = DefaultBoolean.True;
                    });

                    info.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                });


                //settings.Settings.HorizontalScrollBarMode = ScrollBarMode.Visible;


            });

            return settings1;


        }

        public ActionResult ExportTo2()
        {
            TaReportViewNewCustomoseUser tavm = new TaReportViewNewCustomoseUser();
            List<TaNewUserDetails> listta_user = new List<TaNewUserDetails>();
            XlsExportOptionsEx exportOptions = new XlsExportOptionsEx();
            exportOptions.CustomizeSheetHeader += options_CustomizeSheetHeader;
            CustomSummaryEventArgs custsumm = new CustomSummaryEventArgs();
            exportOptions.ExportType = ExportType.WYSIWYG;
            exportOptions.CustomizeCell += new DevExpress.Export.CustomizeCellEventHandler(exportOptions_CustomizeCell);
            
            tavm.TaUserDetails = listta_user;
            return GridViewExtension.ExportToXls(GetWorkingDaysReport(), tavm.TaUserDetails, exportOptions);
        }
        private GridViewSettings GetWorkingDaysReport()
        {

            var settings = new GridViewSettings();

            // Export-specific settings  
            // settings.SettingsExport.ExportedRowType = DevExpress.Web.Export.GridViewExportedRowType.All;
            settings.SettingsExport.FileName = "Report.xls";
            settings.SettingsExport.PaperKind = System.Drawing.Printing.PaperKind.A4;
            settings.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            settings.ControlStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            settings.KeyFieldName = "id";
            settings.Settings.ShowFilterRow = true;
            settings.SettingsExport.Styles.Header.Font.Bold = true;
            settings.SettingsText.EmptyDataRow = " ";
            settings.CommandColumn.CellStyle.BackColor = System.Drawing.Color.Blue;
            settings.CommandColumn.CellStyle.ForeColor = System.Drawing.Color.Transparent;
            settings.Name = "gvBands";
            //settings.CallbackRouteValues = new { Controller = "Columns", Action = "Bands" };
            settings.EnableTheming = false;
            settings.Width = Unit.Percentage(100);
            
            settings.Columns.AddBand(cap =>
            {
                cap.Caption = "Informācija no Elektroniskās darba laika uzskaites sistēmas (EDLUS) reģistrētajiem auditācijas pierakstiem  par periodu _________________________(taksācijas gads, mēnesis)";
                cap.HeaderStyle.Wrap = DefaultBoolean.True;
                cap.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                
                cap.Columns.AddBand(c =>
                {
                    c.Caption = "Nr.p.k.";
                    c.HeaderStyle.Wrap = DefaultBoolean.True;
                    c.HeaderStyle.BackColor = System.Drawing.Color.Transparent;
                    c.HeaderStyle.ForeColor = System.Drawing.Color.Black;                                        
                });

                cap.Columns.AddBand(c =>
                {
                    c.Caption = "Informācijas iesniegšanas datums (dd.mm.gggg.)";
                    c.HeaderStyle.Wrap = DefaultBoolean.True;                    
                });

                cap.Columns.AddBand(info =>
                {
                    info.Caption = "Taksācijas periods";
                    info.Columns.AddBand(c1 =>
                    {
                        c1.Caption = "Datums";
                        c1.HeaderStyle.Wrap = DefaultBoolean.True;
                    });
                    info.Columns.AddBand(c2 =>
                    {
                        c2.Caption = "Mēnesis";
                        c2.HeaderStyle.Wrap = DefaultBoolean.True;
                    });
                    info.Columns.AddBand(c3 =>
                    {
                        c3.Caption = "Gads";
                        c3.HeaderStyle.Wrap = DefaultBoolean.True;
                    });
                    info.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                });

                cap.Columns.AddBand(band1 =>
                {
                    band1.Caption = "Labotie dati jeb auditācijas pieraksti";
                    band1.Columns.AddBand(empdet =>
                    {
                        empdet.Caption = "Nodarbinātais";
                        empdet.Columns.Add("Vārds");
                        
                        empdet.Columns.Add("Uzvārds");
                        empdet.Columns.Add(c =>
                        {
                            c.FieldName = "Personas kods";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;                            
                        });
                        empdet.Columns.AddBand(c =>
                        {
                            c.Caption = "Ja personai nav personas koda";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;

                            c.Columns.Add(c1 =>
                            {
                                c1.FieldName = "Dzimšanas datums, mēnesis, gads, vai vīzas vai uzturēšanās atļaujas numurs";
                                c1.HeaderStyle.Wrap = DefaultBoolean.True;                                
                            });
                        });
                        empdet.Columns.Add("Amats");
                        empdet.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    });

                    band1.Columns.AddBand(initialdates =>
                    {
                        initialdates.Caption = "Sākotnējie dati";
                        initialdates.Columns.Add(c =>
                        {
                            c.FieldName = "Ienākšanas (hh:mm:ss)";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;
                        });
                        initialdates.Columns.Add(c =>
                        {
                            c.FieldName = "Iziešanas (hh:mm:ss)";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;
                        });
                        initialdates.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    });
                    band1.Columns.AddBand(correcteddates =>
                    {
                        correcteddates.Caption = "Labotie dati";
                        correcteddates.Columns.Add(c =>
                        {
                            c.FieldName = "Ienākšanas (hh:mm:ss)";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;
                        });
                        correcteddates.Columns.Add(c =>
                        {
                            c.FieldName = "Iziešanas (hh:mm:ss)";
                            c.HeaderStyle.Wrap = DefaultBoolean.True;
                        });
                        correcteddates.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    });
                    band1.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                });

                cap.Columns.AddBand(c =>
                {
                    c.Caption = "Paskaidrojums";
                    c.HeaderStyle.Wrap = DefaultBoolean.True;
                });
            });
            settings.ControlStyle.BackColor = System.Drawing.Color.AliceBlue;
            settings.ControlStyle.ForeColor = System.Drawing.Color.White;
            return settings;
        }
        #endregion
    }
}

