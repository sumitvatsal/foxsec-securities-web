﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FoxSec.Web.ViewModels.HomeViewModel>" %>
<div id='panel_tab_jobs'>
    <form id="TASettings">

        <table style="margin: 0; width: 90%; padding: 0; border-spacing: 1px;">
            <col style="width: 10%">
            <col style="width: 25%">
            <col style="width: 25%">
            <col style="width: 30%">
            <thead>
                <tr>
                    <th></th>
                    <th><%:ViewResources.SharedStrings.CommonDate %></th>
                    <%//if(Model.User.CompanyId == null )
                        if (Model.User.IsCompanyManager || Model.User.IsSuperAdmin)
                        {%>
                    <th style="padding-right: 100px;"><%:ViewResources.SharedStrings.UsersCompany %></th>
                    <%}%>
                    <%if (Model.User.CompanyId == null || Model.User.CompanyId != null)
                        {%>
                    <th style="padding-right: 50px;"><%:ViewResources.SharedStrings.Department %></th>
                    <%}
                        if (Model.User.IsSuperAdmin)
                        {%>
                    <th style="padding-right: 150px;">Building</th>
                    <%} %>

                    <th style="padding-right: 50px;">Format</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th style="text-align: right"><%:ViewResources.SharedStrings.CommonFrom %></th>
                    <%-- <td><%:Html.TextBox("FromDateTA",  new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd.MM.yyyy"),new { @onchange = "onchangeDateFilter();" })%></td>
                    --%>               <%--  <td><%:Html.TextBox("FromDateTA",new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd.MM.yyyy"),new { @onchange = "onchangeDateFilter()" }) %></td>--%>
                    <td><%:Html.TextBox("FromDateTA", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd.MM.yyyy"),new { @onchange = "onchangefromdate();" }) %></td>
                    <%//if(Model.User.CompanyId == null )
                        if (Model.User.IsCompanyManager || Model.User.IsSuperAdmin)
                        {%>
                    <td>
                        <select style="width: 90%" name="CompanyId" id="CompanyIdta" onchange="CompanyChanged()"></select>

                    </td>
                    <%}%>
                    <%if (Model.User.CompanyId == null || Model.User.CompanyId != null)
                        {%>
                    <td>
                        <select style="width: 90%" name="DepartmentId" id="DepartmentId"></select>
                        <%--onchange="showReportCompany()"--%>
                       
                    </td>
                    <%}%>
                    <%if (Model.User.CompanyId == null)
                        {%>
                    <td style="margin-right: -80px">
                        <select style="width: 90%" name="BuildingId" id="BuildingId"></select>

                    </td>

                    <%}%>
                    <%:Html.HiddenFor(m=>m.User.CompanyId,new { id="hdnCompanyID"}) %>
                    <%:Html.HiddenFor(m=>m.User.IsDepartmentManager,new { id="hdnIsDM"}) %>
                    <%:Html.HiddenFor(m=>m.User.IsCompanyManager,new { id="hdnIsCM"}) %>
                    <td>
                        <select style="width: 90%" name="Format" id="Format">
                            <option value="1">12:59</option>
                            <option value="2">12,99</option>
                        </select>
                        <%--onchange="showReportCompany()"--%>
                       
                    </td>
                </tr>
                <tr>
                    <th style="text-align: right"><%:ViewResources.SharedStrings.CommonTo %></th>
                    <td><%:Html.TextBox("ToDateTA", DateTime.Now.ToString("dd.MM.yyyy"),new { @onchange = "onchangefromdate();" }) %></td>

                    <td>
                        <input type="button" onclick="chkDate();" name="click" id="click" value='<%:ViewResources.SharedStrings.previousmonth %>' /></td>
                    <td>
                        <input type="button" onclick="chkcurrentDate();" name="click1" id="click1" value='<%:ViewResources.SharedStrings.currentmonth %>' /></td>
                </tr>



            </tbody>
        </table>
        <br />
        <br />


        <script type="text/javascript">


            function chkDate() {

                $.ajax({
                    type: "POST",
                    url: "../TAReport/Checkdate/",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        //$.each(data, function (idx, value) {
                        $("#FromDateTA").val(data.som);
                        $("#ToDateTA").val(data.eom);
                        GetBuilding();
                        // });
                    }
                });
            }

            function onchangefromdate() {
                $.ajax({
                    type: "Get",
                    url: "/TAReport/GetBuildingsOnClick_FromDate",
                    dataType: 'json',
                    data: { datefrom: $("#FromDateTA").val(), dateto: $("#ToDateTA").val() },

                    success: function (data) {
                        if ($("select#BuildingId").size() > 0) {
                            $("select#BuildingId").html(data);
                        }
                    }
                });
                return false;

            }



        </script>
        <script>

            $(document).ready(function () {

                var compID = $("#hdnCompanyID").val();
                var isDM = $("#hdnIsDM").val();

                if (compID && isDM) {
                    $.ajax({
                        type: "Get",
                        url: "/Log/GetDepartmentsByCompany",
                        dataType: 'json',
                        data: { id: compID },//$('#CompanyId').val() },
                        success: function (data) {
                            if ($("select#DepartmentId").size() > 0) {
                                $("select#DepartmentId").html(data);
                            }
                        }
                    });
                }

                $.ajax({
                    type: "Get",
                    url: "/Log/GetCompanies",
                    dataType: 'json',

                    success: function (data) {
                        if ($("select#CompanyIdta").size() > 0) {
                            $("select#CompanyIdta").html(data);
                        }
                    }

                });
            })


        </script>
        <script>

            $(document).ready(function () {

                $.ajax({
                    type: "Get",
                    url: "/TAReport/GetBuildings",
                    dataType: 'json',
                    data: { datefrom: $("#FromDateTA").val(), dateto: $("#ToDateTA").val() },

                    success: function (data) {
                        if ($("select#BuildingId").size() > 0) {
                            $("select#BuildingId").html(data);
                        }
                    }

                });
            })


            $(document).ready(function () {
                //$.ajax({
                //    type: "Get",
                //    url: "/Log/GetDepartmentsByCompany",
                //    dataType: 'json',
                //    data: { id: $('#CompanyIdta').val() },
                //    success: function (data) {
                //        if ($("select#DepartmentId").size() > 0) {
                //            $("select#DepartmentId").html(data);
                //        }
                //    }

                //});
            })


        </script>

        <script type="text/javascript">


            function chkcurrentDate() {

                $.ajax({
                    type: "POST",
                    url: "../TAReport/chkcurrentDate/",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        //$.each(data, function (idx, value) {
                        $("#FromDateTA").val(data.som);
                        $("#ToDateTA").val(data.eom);
                        GetBuilding();
                        // });
                    }
                });
            }



        </script>


        <table style='width: 95%'>
            <col style="width: 20%">
            <col style="width: 70%">
            <col style="width: 5%">

            <thead>
                <tr>
                <tr>
                    <th style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.UsersName %></th>
                    <th style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.CommonDescription %></th>
                    <th style='padding: 2px;'></th>
                </tr>
            </thead>
            <tbody>
                <% var i = 1;
                    var bg = "";
                    var issuperadmin = Model.User.IsSuperAdmin;
                    var iscompanymanager = Model.User.IsCompanyManager;
                    var isdepartmentmanager = Model.User.IsDepartmentManager;
                    var canconf = ((Model.User.Menues.IsAvailabe((int)FoxSec.DomainModel.DomainObjects.Menu.ViewTAConfMenu)));
                    var canedit = ((Model.User.Menues.IsAvailabe((int)FoxSec.DomainModel.DomainObjects.Menu.ViewTAReportMenu)));%>

                <%if (issuperadmin)
                    {
                        bg = (i++ % 2 == 1) ? "style='background-color:#CCC;'" : ""; %>

                <%--<tr id="ta_boconf" <%= bg %>>
        <td style='padding:2px; text-align:left'> Entry  </td>
         <td style='padding:2px; text-align:left'> For T&ABuilding Names </td>
        <td style='padding:2px; text-align:right'><span id='button_TA_Bo' class='icon icon_green_go tipsy_we' title='<%= Html.Encode("Create Entry for Building Names") %>' onclick="CreateBuildingNames();" ></span></td>
 </tr> --%>
                <%-- <%  bg = (i++ % 2 == 1) ? "style='background-color:#CCC;'" : "";%>--%>
                <tr id="ta_boconf" style="background-color: #CCC" <%= bg %>>
                    <td style='padding: 2px; text-align: left'>Building Name Schedule   </td>
                    <td style='padding: 2px; text-align: left'>Moving  Registration Devices Building Names </td>
                    <td style='padding: 2px; text-align: right'><span id='button_TA_Bo' class='icon icon_green_go tipsy_we' title='<%= Html.Encode("TABuilding Name Details") %>' onclick="showBuildingNameList();"></span></td>
                </tr>
                <%--<%  bg = (i++ % 2 == 1) ? "style='background-color:#CCC;'" : "";%>
     <tr id="ta_boconf" <%= bg %>>
        <td style='padding:2px; text-align:left'> T&A   </td>
         <td style='padding:2px; text-align:left'> building  Export Report </td>
        <td style='padding:2px; text-align:right'><span id='button_TA_Bo' class='icon icon_green_go tipsy_we' title='<%= Html.Encode("Create Entry for Building Names") %>' onclick= "ExportTaBuildingName();"></span></td>
 </tr>--%>
                <%-- </tr> --%>



                <tr id="ta_boconf" style="background-color: #fff">
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAbulding %> </td>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAlist %> </td>
                    <td style='padding: 2px; text-align: right'><span id='button_TA_Bo' class='icon icon_green_go tipsy_we' title='<%= Html.Encode(ViewResources.SharedStrings.TAlist) %>' onclick='<%=string.Format("javascript:allBuildingObjects(\"submit_edit_user\", {0}, \"{1} {2}\")", 500, Html.Encode("eesn"), Html.Encode("peren")) %>'></span></td>
                </tr>
                <%}%>
                <%--<tr>
        <td style='padding:2px; text-align:left'> 2a. T&A Schedule   </td>
         <td style='padding:2px; text-align:left'> Start from,breaks,Lunch,Until to </td>
        <td style='padding:2px; text-align:right'><span id='button_TA_Bo' class='icon icon_green_go tipsy_we' title='<%= Html.Encode("Create Entry for Building Names") %>' onclick= '<%=string.Format("javascript:EntryFort&A(\"submit_edit_user\", {0}, \"{1} {2}\")", 500, Html.Encode("eesn"), Html.Encode("peren")) %>'   ></span></td>
 </tr> 
      <tr>
        <td style='padding:2px; text-align:left'> Users Schedule   </td>
         <td style='padding:2px; text-align:left'> Activating Users T&A and Schedule </td>
        <td style='padding:2px; text-align:right'><span id='button_TA_Bo' class='icon icon_green_go tipsy_we' title='<%= Html.Encode("Create Entry for Building Names") %>' onclick= '<%=string.Format("javascript:EntryFort&A(\"submit_edit_user\", {0}, \"{1} {2}\")", 500, Html.Encode("eesn"), Html.Encode("peren")) %>'   ></span></td>
 </tr> --%>
                <%if (issuperadmin == false && !iscompanymanager == false && !isdepartmentmanager == false)
                    { %>

                <tr id="ta_detailed_report" style="background-color: #CCC" <%= bg %>>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAMyReport %> </td>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAMyReport  %> </td>
                    <td style='padding: 2px; text-align: right'><span id='button_TA_detailed_report' class='icon icon_green_go tipsy_we' title='<%= Html.Encode(ViewResources.SharedStrings.TAreportViewe) %>' onclick='<%=string.Format("javascript:MyTAReport(\"submit_edit_user\", {0}, \"{1} {2}\")", 3, Html.Encode("eesn"), Html.Encode("peren")) %>'></span></td>
                </tr>
                <%} %>
                <%if (issuperadmin || iscompanymanager || isdepartmentmanager)
                    {
                        bg = (i++ % 2 == 1) ? "style='background-color:#CCC;'" : "";%>
                <tr id="ta_mounth_report" style="background-color: #CCC" <%--<%= bg %>--%>>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAMonthreport %> </td>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.WorkMontReportDescription %> </td>
                    <td style='padding: 2px; text-align: right'><span class='icon icon_green_go tipsy_we' title='<%= Html.Encode(ViewResources.SharedStrings.WorkMontReportDescription) %>' onclick='<%=string.Format("javascript:OpenMonthReport(\"submit_edit_user\", {0}, \"{1} {2}\")", 500, Html.Encode("eesn"), Html.Encode("peren")) %>'></span></td>

                </tr>

                <%     bg = (i++ % 2 == 1) ? "style='background-color:#CCC;'" : "";%>
                <tr id="ta_mounth_report" style="background-color: #FFF">
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAlist %> </td>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAlistDetail %> </td>
                    <td style='padding: 2px; text-align: right'><span id='button_TA_mounthreport' class='icon icon_green_go tipsy_we' title='<%= Html.Encode(ViewResources.SharedStrings.WorkMontReportDescription) %>' onclick='<%=string.Format("javascript:StartEndReport(\"submit_edit_user\", {0}, \"{1} {2}\")", 500, Html.Encode("eesn"), Html.Encode("peren")) %>'></span></td>
                </tr>
                <%bg = (i++ % 2 == 1) ? "style='background-color:#CCC;'" : "";%>
                <tr id="ta_detailed_report" style="background-color: #CCC" <%--<%= bg %>--%>>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TADetailReport %> </td>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAdetails %> </td>
                    <td style='padding: 2px; text-align: right'><span id='button_TA_detailed_report' class='icon icon_green_go tipsy_we' title='<%= Html.Encode(ViewResources.SharedStrings.TAdetails) %>' onclick='<%=string.Format("javascript:TAReportDetailed(\"submit_edit_user\", {0}, \"{1} {2}\")", 3, Html.Encode("eesn"), Html.Encode("peren")) %>'></span></td>
                </tr>
                <%bg = (i++ % 2 == 1) ? "style='background-color:#CCC;'" : "";%>
                <tr id="ta_detailed_report" style="background-color: #FFF" <%--<%= bg %>--%>>

                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAExport %> </td>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAExportTo %> </td>
                    <td style='padding: 2px; text-align: right'><span id='button_TA_detailed_report' class='icon icon_green_go tipsy_we' title='<%= Html.Encode(ViewResources.SharedStrings.TAExportTo) %>' onclick='<%=string.Format("javascript:TAReportExport(\"submit_edit_user\", {0}, \"{1} {2}\")", 3, Html.Encode("eesn"), Html.Encode("peren")) %>'></span></td>
                </tr>
                <%} %>


                <tr id="ta_mounth_report" style="background-color: #CCC">
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAMonthreportWorkingDays %> </td>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAMonthreportWorkingDaysDesc %> </td>
                    <td style='padding: 2px; text-align: right'><span id='button_TA_mounthreport' class='icon icon_green_go tipsy_we' title='<%= Html.Encode(ViewResources.SharedStrings.TAMonthreportWorkingDaysDesc) %>' onclick='<%=string.Format("javascript:OpenMonthReportWorkingDays(\"submit_edit_user\", {0}, \"{1} {2}\")", 500, Html.Encode("eesn"), Html.Encode("peren")) %>'></span></td>
                </tr>
                <%bg = (i++ % 2 == 1) ? "style='background-color:#CCC;'" : "";%>
                <tr id="ta_tax_report" style="background-color: #FFF" <%--<%= bg %>--%>>
                    <td style='padding: 2px; text-align: left'>Tax Report</td>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAMonthreportWorkingDaysDesc %> </td>
                    <td style='padding: 2px; text-align: right'><span id='button_TA_taxreport' class='icon icon_green_go tipsy_we' title='<%= Html.Encode(ViewResources.SharedStrings.TAMonthreportWorkingDaysDesc) %>' onclick='<%=string.Format("javascript:Taxreport()", 500, Html.Encode("eesn"), Html.Encode("peren")) %>'></span></td>
                </tr>
                  <tr id="daily_work_report" style="background-color: #CCC">
                    <td style='padding: 2px; text-align: left'>Daily Working Report </td>
                    <td style='padding: 2px; text-align: left'><%:ViewResources.SharedStrings.TAMonthreportWorkingDaysDesc %> </td>
                    <td style='padding: 2px; text-align: right'><span id='button_TA_dailywork' class='icon icon_green_go tipsy_we' title='<%= Html.Encode(ViewResources.SharedStrings.TAMonthreportWorkingDaysDesc) %>' onclick='<%=string.Format("javascript:DailyWorkingReport()", 500, Html.Encode("eesn"), Html.Encode("peren")) %>'></span></td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" id="newcompnayid" />
    </form>




    <script type="text/javascript">

        $(document).ready(function () {
            //GetCompanies();
            var i = $('#panel_owner li').index($('#TAReportTab'));
            var opened_tab = '<%: Session["tabIndex"] %>';
            if (opened_tab != '') {
                i1 = new Number(opened_tab);
                if (i1 != i) {
                    SetOpenedTab(i);
                }
            }
            else {
                SetOpenedTab(i);
            }
            $("input:button").button();
            $('div#Work').fadeIn("slow");

            $(".tipsy_we").attr("class", function () {
                $(this).tipsy({ gravity: $.fn.tipsy.autoWE, html: true });
            });

            $("#FromDateTA").datepicker({
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                maxdate: "1d",
                onClose: function (selectedDate) {
                    $("#ToDateTA").datepicker("option", "minDate", selectedDate);
                }
                //onSelect: function (dateText, inst) {
                //    $("#ToDateTA").datepicker("option", "minDate", dateText);
                //}
            });


            $("#ToDateTA").datepicker({
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onClose: function (selectedDate) {
                    $("#FromDateTA").datepicker("option", "maxDate", selectedDate);
                }
            });

            //bindCampany();
            //GetBuilding();
            //CompanyChanged();
        });

        function showBuildingNameList() {

            $("div#modal-dialog").dialog({
                open: function () {

                    var buildingid = $("#BuildingId").val();
                    if (buildingid == "") {
                        buildingid = "0";
                    }
                    $('body').css('overflow', 'auto');
                    $("div#user-modal-dialog").html("");
                    $("div#modal-dialog").html("<div id='AreaUserEditWait' style='width: 100%; height:580px; text-align:center'><span style='position:relative; top:45%' class='icon loader'></span></div>");
                    $.ajax({
                        type: 'GET',
                        url: '/TAReport/TABuildingList',
                        cache: false,
                        data: {
                            datefrom: $("#FromDateTA").val(),
                            dateto: $("#ToDateTA").val(),
                            buildingid: buildingid
                        },
                        success: function (html) {
                            $("div#modal-dialog").html(html);
                        }
                    });
                    $(this).parents('.ui-dialog-buttonpane button:eq(0)').focus();
                },
                resizable: false,
                width: 1200,
                height: 710,
                modal: true,
                title: "<span class='ui-icon ui-icon-home' style='float:left; margin:1px 5px 0 0'></span>" + "TABuilding Name List",
                buttons: {
                    '<%= ViewResources.SharedStrings.BtnClose %>': function () {
                    $(this).dialog("close");
                }
                },
                close: function () {
                    $('body').css('overflow', 'hidden');
                }
            });
        return false;
    }

    function CompanyChanged() {

        var cid = $('#CompanyIdta').val();
        $('#newcompnayid').val(cid);
        $.ajax({
            type: "Get",
            url: "/Log/GetDepartmentsByCompany",
            dataType: 'json',
            data: { id: cid },//$('#CompanyId').val() },
            success: function (data) {
                if ($("select#DepartmentId").size() > 0) {
                    $("select#DepartmentId").html(data);
                }
            }
        })


        return false;

    }
    function allBuildingObjects(Action, UserId, Username) {
        $("div#modal-dialog").dialog({
            open: function () {
                $("div#user-modal-dialog").html("");
                $("div#modal-dialog").html("<div id='AreaUserEditWait' style='width: 100%; height:580px; text-align:center'><span style='position:relative; top:45%' class='icon loader'></span></div>");
                $.ajax({
                    type: 'GET',
                    url: '/TAReport/OpenAllBuildingsObjects',
                    cache: false,
                    data: {
                        id: UserId
                    },
                    success: function (html) {
                        $("div#modal-dialog").html(html);
                    }
                });
                $(this).parents('.ui-dialog-buttonpane button:eq(0)').focus();
            },
            resizable: false,
            width: 900,
            height: 710,
            modal: true,
            title: "<span class='ui-icon ui-icon-home' style='float:left; margin:1px 5px 0 0'></span>" + '<%:ViewResources.SharedStrings.TAbulding%>',
                buttons: {
                    '<%= ViewResources.SharedStrings.BtnClose %>': function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        }
        function GetBuilding() {
            $.ajax({
                type: "Get",
                url: "/TAReport/GetBuildings",
                dataType: 'json',
                data: { datefrom: $("#FromDateTA").val(), dateto: $("#ToDateTA").val() },

                success: function (data) {
                    if ($("select#BuildingId").size() > 0) {
                        $("select#BuildingId").html(data);
                    }
                }
            });
            return false;
        }




        function TAReportDetailed(Action, UserId, Username) {

            $("div#modal-dialog").dialog({
                open: function () {
                    $("div#user-modal-dialog").html("");
                    $("div#modal-dialog").html("<div id='AreaUserEditWait' style='width: 100%; height:580px; text-align:center'><span style='position:relative; top:45%' class='icon loader'></span></div>");
                    $.ajax({
                        type: 'GET',
                        url: '/TAReport/TAReportDetailed',
                        cache: false,
                        data: {
                            company: $('#CompanyIdta').val(),
                            department: $('#DepartmentId').val(),
                            id: UserId,
                            FromDateTA: $('#FromDateTA').val(),
                            ToDateTA: $("#ToDateTA").val(),
                            BuildingId: $("#BuildingId").val()
                        },
                        success: function (html) {
                            $("div#modal-dialog").html(html);
                        }
                    });
                    $(this).parents('.ui-dialog-buttonpane button:eq(0)').focus();
                },
                resizable: false,
                width: 1000,
                height: 710,
                modal: true,
                title: "<span class='ui-icon ui-icon-home' style='float:left; margin:1px 5px 0 0'></span>" + '<%= ViewResources.SharedStrings.TADetailReport %>',
                buttons: {
                    '<%= ViewResources.SharedStrings.BtnClose %>': function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        }

        function TAReportExport(Action, UserId, Username) {

            var date1 = $('#FromDateTA').val();
            $("div#modal-dialog").dialog({
                open: function () {
                    var BName = $("#BuildingId :selected").text();

                    if (BName == "") {
                        BName = "0";
                    }
                    var cc = $("#newcompnayid").val();

                    $("div#user-modal-dialog").html("");
                    $("div#modal-dialog").html("<div id='AreaUserEditWait' style='width: 100%; height:580px; text-align:center'><span style='position:relative; top:45%' class='icon loader'></span></div>");
                    $.ajax({
                        type: 'GET',
                        url: '/TAReport/TAReportExport',
                        cache: false,
                        data: {
                            department: $('#DepartmentId').val(),
                            company: $('#CompanyIdta').val(),
                            FromDateTA: $('#FromDateTA').val(),
                            ToDateTA: $("#ToDateTA").val(),
                            format: $("#Format").val(),
                            BuildingId: $("#BuildingId").val(),
                            BName: BName
                        },
                        success: function (html) {
                            $("div#modal-dialog").html(html);
                        }
                    });
                    $(this).parents('.ui-dialog-buttonpane button:eq(0)').focus();
                },
                resizable: false,
                width: 1000,
                height: 710,
                modal: true,
                title: "<span class='ui-icon ui-icon-home' style='float:left; margin:1px 5px 0 0'></span>" + '<%= ViewResources.SharedStrings.TAExport%>',
                buttons: {
                    '<%= ViewResources.SharedStrings.BtnClose %>': function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        }
        function MyTAReport(Action, UserId, Username) {
            $("div#modal-dialog").dialog({
                open: function () {
                    $("div#user-modal-dialog").html("");
                    $("div#modal-dialog").html("<div id='AreaUserEditWait' style='width: 100%; height:580px; text-align:center'><span style='position:relative; top:45%' class='icon loader'></span></div>");
                    $.ajax({
                        type: 'GET',
                        url: '/TAReport/MyTAReport',
                        cache: false,
                        data: {
                            format: $('#Format').val(),
                            company: $('#CompanyIdta').val(),
                            id: UserId,
                            FromDateTA: $('#FromDateTA').val(),
                            ToDateTA: $("#ToDateTA").val()
                        },
                        success: function (html) {
                            $("div#modal-dialog").html(html);
                        }
                    });
                    $(this).parents('.ui-dialog-buttonpane button:eq(0)').focus();
                },
                resizable: false,
                width: 1000,
                height: 710,
                modal: true,
                title: "<span class='ui-icon ui-icon-home' style='float:left; margin:1px 5px 0 0'></span>" + '<%= ViewResources.SharedStrings.TAMyReport %>',
                buttons: {
                    '<%= ViewResources.SharedStrings.BtnClose %>': function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        }

        function StartEndReport(Action, UserId, Username) {
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.9;
            $("div#modal-dialog").dialog({
                open: function () {
                    $("div#user-modal-dialog").html("");
                    $("div#modal-dialog").html("<div id='AreaUserEditWait' style='width: 100%; height:580px; text-align:center'><span style='position:relative; top:45%' class='icon loader'></span></div>");
                    $.ajax({
                        type: 'GET',
                        url: '/TAReport/StartEndReport',
                        cache: false,
                        data: {
                            format: $('#Format').val(),
                            company: $('#CompanyIdta').val(),
                            department: $('#DepartmentId').val(),
                            id: UserId,
                            FromDateTA: $('#FromDateTA').val(),
                            ToDateTA: $("#ToDateTA").val()
                        },
                        success: function (html) {
                            $("div#modal-dialog").html(html);
                        }
                    });
                    $(this).parents('.ui-dialog-buttonpane button:eq(0)').focus();
                },
                resizable: false,
                width: dWidth,
                height: 710,
                modal: true,
                title: "<span class='ui-icon ui-icon-home' style='float:left; margin:1px 5px 0 0'></span>" + '<%= ViewResources.SharedStrings.TAlist %>',
                buttons: {
                    '<%= ViewResources.SharedStrings.BtnClose %>': function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        }
        function test() { }
        function OpenMonthReport(Action, UserId, Username) {
            //var buildingID = $('#BuildingId').val();
            //alert("value:" + buildingID);

            var wWidth = $(window).width();
            var dWidth = wWidth * 0.9;
            $("div#modal-dialog").dialog({
                open: function () {
                    $("div#user-modal-dialog").html("");
                    $("div#modal-dialog").html("<div id='AreaUserEditWait'style='width: 100%; height:580px; text-align:center'><span style='position:relative; top:45%' class='icon loader'></span></div>");
                    $.ajax({
                        type: 'GET',
                        url: '/TAReport/OpenMounthReport',
                        cache: false,
                        data: {
                            format: $('#Format').val(),
                            company: $('#CompanyIdta').val(),
                            department: $('#DepartmentId').val(),
                            id: UserId,
                            FromDateTA: $('#FromDateTA').val(),
                            ToDateTA: $("#ToDateTA").val(),
                            BuildingId: $('#BuildingId').val()
                        },
                        success: function (html) {

                            $("div#modal-dialog").html(html);
                        }
                    });
                    $(this).parents('.ui-dialog-buttonpane button:eq(0)').focus();
                },
                resizable: true,
                width: dWidth,
                height: 710,
                modal: true,
                title: "<span class='ui-icon ui-icon-home' style='float:left; margin:1px 5px 0 0'></span>" + '<%= ViewResources.SharedStrings.TAMonthreport%>',
                buttons: {
                    '<%= ViewResources.SharedStrings.BtnClose %>': function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        }


        function OpenMonthReportWorkingDays(Action, UserId, Username) {
            //var buildingID = $('#BuildingId').val();
            //alert("value:" + buildingID);

            var wWidth = $(window).width();
            var dWidth = wWidth * 0.9;
            $("div#modal-dialog").dialog({
                open: function () {
                    $("div#user-modal-dialog").html("");
                    $("div#modal-dialog").html("<div id='AreaUserEditWait'style='width: 100%; height:580px; text-align:center'><span style='position:relative; top:45%' class='icon loader'></span></div>");
                    $.ajax({
                        type: 'GET',
                        url: '/TAReport/OpenMounthReportWrokingDays',
                        cache: false,
                        data: {
                            format: $('#Format').val(),
                            company: $('#CompanyIdta').val(),
                            department: $('#DepartmentId').val(),
                            id: UserId,
                            FromDateTA: $('#FromDateTA').val(),
                            ToDateTA: $("#ToDateTA").val(),
                            BuildingId: $('#BuildingId').val()
                        },
                        success: function (html) {

                            $("div#modal-dialog").html(html);
                        }
                    });
                    $(this).parents('.ui-dialog-buttonpane button:eq(0)').focus();
                },
                resizable: true,
                width: dWidth,
                height: 710,
                modal: true,
                title: "<span class='ui-icon ui-icon-home' style='float:left; margin:1px 5px 0 0'></span>" + '<%= ViewResources.SharedStrings.TAMonthreportWorkingDays%>',
                buttons: {
                    '<%= ViewResources.SharedStrings.BtnClose %>': function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        }

        function Taxreport() {
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.9;
            $("div#modal-dialog").dialog({
                open: function () {
                    $("div#user-modal-dialog").html("");
                    $("div#modal-dialog").html("<div id='AreaUserEditWait'style='width: 100%; height:580px; text-align:center'><span style='position:relative; top:45%' class='icon loader'></span></div>");
                    $.ajax({
                        type: 'GET',
                        url: '/TAReport/Bands',
                        cache: false,
                        data: {},
                        success: function (html) {
                            $("div#modal-dialog").html(html);
                        }
                    });
                    $(this).parents('.ui-dialog-buttonpane button:eq(0)').focus();
                },
                resizable: true,
                width: dWidth,
                height: 710,
                modal: true,
                title: "<span class='ui-icon ui-icon-home' style='float:left; margin:1px 5px 0 0'></span>" + '<%= ViewResources.SharedStrings.TAMonthreportWorkingDays%>',
                buttons: {
                    '<%= ViewResources.SharedStrings.BtnClose %>': function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        }

        function DailyWorkingReport() {
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.9;
            $("div#modal-dialog").dialog({
                open: function () {
                    $("div#user-modal-dialog").html("");
                    $("div#modal-dialog").html("<div id='AreaUserEditWait'style='width: 100%; height:580px; text-align:center'><span style='position:relative; top:45%' class='icon loader'></span></div>");
                    $.ajax({
                        type: 'GET',
                        url: '/TAReport/DailyWorkingReport',
                        cache: false,
                        data: {},
                        success: function (html) {
                            $("div#modal-dialog").html(html);
                        }
                    });
                    $(this).parents('.ui-dialog-buttonpane button:eq(0)').focus();
                },
                resizable: true,
                width: dWidth,
                height: 710,
                modal: true,
                title: "<span class='ui-icon ui-icon-home' style='float:left; margin:1px 5px 0 0'></span>" + '<%= ViewResources.SharedStrings.TAMonthreportWorkingDays%>',
                buttons: {
                    '<%= ViewResources.SharedStrings.BtnClose %>': function () {
                        $(this).dialog("close");
                    }
                }
            });
            return false;
        }
    </script>
    <%--<style type="text/css">
        .ui-dialog .ui-dialog-title {
            direction: ltr;
            float: left;
            margin: 0.1em 0 0.2em 16px;
        }

        .ui-dialog .ui-dialog-titlebar-close {
            left: 0.3em;
        }

        .ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset {
            float: none;
        }

        .ui-dialog .ui-dialog-buttonpane {
            text-align:center;/*!important*//* left/center/right */
        }
    </style>--%>
</div>
