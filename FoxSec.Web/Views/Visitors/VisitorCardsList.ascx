﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FoxSec.Web.ViewModels.UserAccessUnitListViewModel>" %>
<table id="userCardsTable" cellpadding="1" cellspacing="0" style="margin: 0; width: 100%; padding: 1px; border-spacing: 0">
    <tr>
        <%--  <th style='width:5%; padding: 2px;'></th>--%>
        <th style='width: 20%; padding: 2px;'><%:ViewResources.SharedStrings.CardsCardType %></th>
        <th style='width: 20%; padding: 2px;'><%:ViewResources.SharedStrings.CardCode %></th>
        <th style='width: 20%; padding: 2px;'><%:ViewResources.SharedStrings.UsersValidation %></th>
        <th style='width: 10%; padding: 2px;'><%:ViewResources.SharedStrings.CommomStatus %></th>
       <%-- <th style='width: 10%; padding: 2px;'></th>--%>
    </tr>
    <% foreach (var card in Model.Cards)
        { %>
    <tr>
        <td style='width: 20%; padding: 2px;'>
            <%= Html.Encode(card.TypeName != null && card.TypeName.Length > 30 ? card.TypeName.Substring(0, 27) + "..." : card.TypeName)%>
        </td>
        <td style='width: 20%; padding: 2px;'>
              <%= Html.Encode(card.FullCardCode) %>
        </td>
        <td style='width: 20%; padding: 2px;'>
            <%= Html.Encode(card.ValidFromStr) %> - <%= Html.Encode(card.ValidToStr) %>
        </td>
        <td style='width: 10%; padding: 2px;'>
            <%= Html.Encode(card.CardStatus) %>
        </td>
      <%--  <td style='width: 10%; padding: 2px;'>
            <span id='button_card_edit_<%= card.Id %>' class='icon icon_green_go tipsy_we' original-title='<%=string.Format("{0} {1}!", ViewResources.SharedStrings.BtnEdit, card.Name) %>' onclick="javascript:EditCard(<%= card.Id %>);"></span>
        </td>--%>
    </tr>
    <% break;
        } %>
</table>
<script type="text/javascript" language="javascript">
    function EditCard(cardId) {
        var visitorid = $('#tab_edit_user_personal_data').find('#Id').attr('value');
        $("div#modal-dialog-2").dialog({
            open: function () {
                // $("div#modal-dialog").html("");
                // $("div#user-modal-dialog").html("");
                $.get('/Visitors/EditCard', { id: cardId }, function (html) {
                    $("div#modal-dialog-2").html(html);
                });
            },
            resizable: false,
            width: 380,
            height: 430,
            modal: true,
            title: "<span class='ui-icon ui-icon-pencil' style='float:left; margin:1px 5px 0 0'></span>" + '<%=ViewResources.SharedStrings.CardsEditCard %>',
            buttons: {
            '<%=ViewResources.SharedStrings.BtnSave %>': function () {
                    $.post("/Visitors/EditCardDet", $("#editCard").serialize()).done(function (data) {
                        $.get('/Visitors/VisitorCardsList', { vid: visitorid, filter: 1 }, function (html) { $("div#VisitorCardsList").html(html); });
                    });
                    ShowDialog('<%=ViewResources.SharedStrings.CommonSaving %>', 2000, true);
                    $(this).dialog("close");
                },
            '<%=ViewResources.SharedStrings.BtnCancel %>': function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                $("div#modal-dialog-2").html("");
                setTimeout(null, 1000);
                $.get('/Visitors/VisitorCardsList', { vid: visitorid, filter: 1 }, function (html) { $("div#VisitorCardsList").html(html); });
            }
        });
        return false;
    }
</script>
