﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FoxSec.Web.ViewModels.UserAccessUnitEditViewModel>" %>
<form id="createNewCard" action="">
    <table cellpadding="1" cellspacing="0" style="margin: 0; width: 100%; padding: 1px; border-spacing: 0">
        <tr>
            <td style='width: 30%; padding: 2px; text-align: right;'>
                <label for='SelectedTypeId'><%:ViewResources.SharedStrings.CardsCardType %>:</label></td>
            <td style='width: 70%; padding: 2px;'><%=Html.DropDownList("TypeId", Model.Card.CardTypes)%></td>
        </tr>
        <tr>
            <td style='width: 30%; padding: 2px; text-align: right;'>
                <label><%:ViewResources.SharedStrings.CardsSerDk %>:</label></td>
            <td style='width: 70%; padding: 2px;'>
                <%=Html.TextBox("Serial", Model.Card.Serial, new { @style = "width:30px", onkeyup="javascript:CardSerValidation();", maxlength = '3' })%>
			+<%=Html.TextBox("Dk", Model.Card.Dk, new { @style = "width:47px", onkeyup="javascript:CardDkValidation();", maxlength = '5' })%>
                <%= Html.ValidationMessage("Serial", null, new { @class = "error" })%>
                <%= Html.ValidationMessage("Dk", null, new { @class = "error" })%>
			
            </td>
        </tr>
        <tr>
            <td style='width: 30%; padding: 2px; text-align: right;'>
                <label><%:ViewResources.SharedStrings.CardsCardCode %>:</label></td>
            <td style='width: 70%; padding: 2px;'>
                <%=Html.TextBox("Code", Model.Card.Code, new { @style = "width:148px", onkeyup = "javascript:CardCodeValidation();" })%>
                <%= Html.ValidationMessage("Code", null, new { @class = "error" })%>
            </td>
        </tr>
        <tr id="cardValidFromRow">
            <td style='width: 30%; padding: 2px; text-align: right;'>
                <label><%:ViewResources.SharedStrings.CardsValidFrom %>:</label></td>
            <td style='width: 70%; padding: 2px;'>
                <%=Html.TextBox("ValidFromStr", Model.Card.ValidFromStr, new { @id = "validFromStr", @style = "width:90px", @class = "date_start" })%>
                <%= Html.ValidationMessage("ValidFromStr", null, new { @class = "error" })%>
            </td>
        </tr>
        <tr id="cardValidToRow">
            <td style='width: 30%; padding: 2px; text-align: right;'>
                <label><%:ViewResources.SharedStrings.CardsValidTo %>:</label></td>
            <td style='width: 70%; padding: 2px;'>
                <%=Html.TextBox("ValidToStr", Model.Card.ValidToStr, new { @id = "validToStr", @style = "width:90px", @class = "date_end" })%>
                <%= Html.ValidationMessage("ValidToStr", null, new { @class = "error" })%>
            </td>
        </tr>
        <% if (!Model.isCompanyManager)
            { %><tr>
            <% }
                                               else
                                               { %>
        <tr style='display: none'>
            <% } %>
            <td style='width: 30%; padding: 2px; text-align: right;'>
                <label for='SelectedCompanyId'><%:ViewResources.SharedStrings.UsersCompany %>:</label></td>
            <td style='width: 70%; padding: 2px;'><%=Html.DropDownList("CompanyId", Model.Card.Companies, new { onchange = "javascript:ChangeCompany(this)", style = "width:72%" })%></td>
        </tr>
        <tr>
            <td style='width: 30%; padding: 2px; text-align: right;'>
                <label><%:ViewResources.SharedStrings.CommonBuilding %>:</label></td>
            <td style='width: 70%; padding: 2px;'><%=Html.DropDownList("BuildingId", new SelectList(Model.Card.Buildings, "Value", "Text", Model.Card.BuildingId))%></td>
        </tr>
        <tr>
            <td style='width: 30%; padding: 2px; text-align: right; vertical-align: top;'>
                <label for='FirstName'><%:ViewResources.SharedStrings.UsersFirstName %>:</label></td>
            <td style='width: 70%; padding: 2px;'>
                <%=Html.TextBox("FirstName", Model.Card.FirstName, new { @style = "width:70%" })%>
                <%= Html.ValidationMessage("FirstName", null, new { @class = "error" })%>
            </td>
        </tr>
        <tr>
            <td style='width: 30%; padding: 2px; text-align: right; vertical-align: top;'>
                <label for='LastName'><%:ViewResources.SharedStrings.UsersLastName %>:</label></td>
            <td style='width: 70%; padding: 2px;'>
                <%=Html.TextBox("LastName", Model.Card.LastName, new { @style = "width:70%" })%>
                <%= Html.ValidationMessage("LastName", null, new { @class = "error" })%>
            </td>
        </tr>
        <tr>
            <td style='width: 30%; padding: 2px; text-align: right; vertical-align: top;'>
                <label for='PersonalCode'><%:ViewResources.SharedStrings.UsersPersonalCode %>:</label></td>
            <td style='width: 70%; padding: 2px;'>
                <%=Html.TextBox("PersonalCode", Model.Card.PersonalCode, new { @style = "width:148px" })%>
                <%= Html.ValidationMessage("PersonalCode", null, new { @class = "error" })%>
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript" language="javascript">

    $(document).ready(function () {
        $('#BuildingId option[value="' + BuildingId + '"]').attr('selected', 'selected');
        $("#CompanyId").val(BuildingId);
        if (CardSer != "" && $("input#Serial").attr('value') == "") {
            $("input#Serial").val(CardSer);
        }
        if (CardDk != "" && $("input#Dk").attr('value') == "") {
            $("input#Dk").val(CardDk);
        }
        if (CardNo != "" && $("input#Code").attr('value') == "") {
            $("input#Code").val(CardNo);
        }
        if (ValidFrom != "" && $("#validFromStr").attr('value') == "") {
            $("#validFromStr").val(ValidFrom);
        }
        if (ValidTo != "" && $("#validToStr").attr('value') == "") {
            $("#validToStr").val(ValidTo);
        }

        $(".date_start").datepicker({
            dateFormat: "dd.mm.yy",
            firstDay: 1,
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            gotoCurrent: ValidFrom == "",
            minDate: 'Y',
            onSelect: function (dateText, inst) {
                $(".date_end").datepicker("option", "minDate", dateText);
            }
        });

        $(".date_end").datepicker({
            dateFormat: "dd.mm.yy",
            firstDay: 1,
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            gotoCurrent: ValidTo == ""
        });
    });

    function CardSerValidation() {

        digitsValidate('Serial');
        if ($('#Serial').attr('value') > 255) {
            $('#Serial').attr('value', 255);
        }
        if ($('#Serial').attr('value').length == 3) {
            $("#Dk").focus();
        }
        if ($('#Serial').attr('value').length != 0) {
            $("#Code").attr('value', "");
        }
        return false;
    }

    function CardDkValidation() {

        digitsValidate('Dk');
        if ($('#Dk').attr('value') > 65535) {
            $('#Dk').attr('value', 65535);
        }
        if ($('#Dk').attr('value').length != 0) {
            $("#Code").attr('value', "");
        }
        return false;
    }

    function CardCodeValidation() {
        //digitsValidate('Code');
        if ($('#Code').attr('value').length != 0) {
            $("#Serial").attr('value', "");
            $("#Dk").attr('value', "");
        }
        return false;
    }

    function ChangeCompany(cntr) {

        comp_id = $(cntr).val();
        $.get('/Card/GetBuildingsByCompany', { companyId: comp_id }, function (data) { $("select#BuildingId").html(data); }, 'json');
    }

</script>
