﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FoxSec.Web.ViewModels.CompanyEditViewModel>" %>
<div id="content_add_company" style='margin:10px; text-align:center; display:none' >
<form id="createNewCompany" action="">
<table width="100%">
    <tr>
		<td style='width:30%; padding:0 5px; text-align:right;'><label for='Add_company_title'><%:ViewResources.SharedStrings.CompaniesCompanyTitle %></label></td>
		<td style='width:70%; padding:0 5px;'>
			<%=Html.TextBox("Company.Name", Model.Company.Name, new { @style = "width:80%" })%>
			<%= Html.ValidationMessage("Company.Name", null, new { @class = "error" })%>
			<%=Html.HiddenFor(m=>m.Company.Id) %>
		</td>
    </tr>
    <tr>
        <td style='width:30%; padding:0 5px; text-align:right;'><label for='Add_company_info'><%:ViewResources.SharedStrings.CompaniesAdditionalInfo %></label></td>
        <td style='width:70%; padding:0 5px;'>
			<%=Html.TextArea("Company.Comment", Model.Company.Comment, new { @style = "width:80%;height:50px;" })%>
			<%= Html.ValidationMessage("Company.Comment", null, new { @class = "error" })%>
		</td>
    </tr>
    <tr>
		  <td style='width:30%; padding:0 5px; text-align:right;'><label for='Add_can_use_cards'><%:ViewResources.SharedStrings.CompaniesCanUseOwnCards %></label></td>
		  <td style='width:70%; padding:0 5px;'><%=Html.CheckBox("Company.IsCanUseOwnCards", Model.Company.IsCanUseOwnCards)%></td>
    </tr>
</table>
    <div id="panel_company_edit">
        <ul>
            <li><a href="#tab_company_buildings"><%:ViewResources.SharedStrings.BuildingsTabName %></a></li>
        </ul>
        <div id="tab_company_buildings">
			<div id="buildings_list">
				<% Html.RenderPartial("BuildingList", Model); %>
			</div>
		</div>
    </div>
</form>
</div>

<script type="text/javascript" language="javascript">

	$(document).ready(function () {
		$("div#panel_company_edit").tabs({
		    beforeLoad: function( event, ui )  {
		        ui.ajaxSettings.async= false,
				ui.ajaxSettings.error=function (xhr, status, index, anchor) {
					$(anchor.hash).html("Couldn't load this tab!");
				}
			},
			fx: {
				opacity: "toggle",
				duration: "fast"
			},
			active: 0
		});

		$("div#content_add_company").fadeIn("300");

		CheckDeleteButtons();
		SetUpCompanyBuildingsNames();
	});

    function IsLast(id) {
        return $("#buildings_list > div").length == id;
    }

    function SaveNewCompanyFloors(id) {
        var floors = new Array();
        $("input[id^=floorCB_][type='checkbox']").each(function () {
            if ($(this).is(':checked') == true) {
                var s = (id + "_" + $(this).attr('id')).split("_");
                floors.push(s[0] + "." + s[2] + "." + s[3]);
            }
        });

        $.ajax({
            type: "POST",
            url: "/Company/SaveFloors",
            data: { Id: -1, values: floors },
            success: function (result) { },
            dataType: "json",
            traditional: true
        });
    }

</script>