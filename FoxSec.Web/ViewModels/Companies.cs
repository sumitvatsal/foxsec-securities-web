﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Configuration;
using FoxSec.DomainModel.DomainObjects;

namespace FoxSec.Web.ViewModels
{
    public class Connection
    {
        public static string connstring = ConfigurationManager.ConnectionStrings["FoxSecDBContext"].ConnectionString;
    }


    public class TaReport1
    {
        public int UserId { get; set; }
        public int BuildingId { get; set; }
    }
    public class Companies
    {
        [Key]
        public int Id { get; set; }

        public int? ParentId { get; set; }

        public int? ClassificatorValueId { get; set; }

        public string Name { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedLast { get; set; }

        public string Comment { get; set; }

        public bool Active { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsCanUseOwnCards { get; set; }
        [Timestamp]
        public byte[] Timestamp { get; set; }

    }
    public class Custormer
    {
        [Key]
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public Nullable<int> CountryId { get; set; }
        public Nullable<int> CityId { get; set; }

        //public virtual City City { get; set; }
        //public virtual Country Country
        //{
        //    get; set;
        //}
    }
    public class Departments
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }

        public int CompanyId { get; set; }
    }

    public class Buildings
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int? TimediffGMTMinutes { get; set; }
        public string AdressStreet { get; set; }
        public string AdressHouse { get; set; }
        public string AdressIndex { get; set; }
        public int LocationId { get; set; }
        public bool IsDeleted { get; set; }
        public int Floors { get; set; }
        public string TimezoneId { get; set; }
    }
    public class TABuildingNames
    {
        [Key]
        public int Id { get; set; }
        public int BuildingId { get; set; }
        public string Name { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string Address { get; set; }
        public string BuildingLicense { get; set; }
        public string CadastralNr { get; set; }
        public bool IsDeleted { get; set; }
        public string Customer { get; set; }
        public string Contractor { get; set; }
        public string Contract { get; set; }
        public string Sum { get; set; }
    }
    public class specificUser
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public class Users
    {
        [Key]
        public int Id { get; set; }

        public string LoginName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string PersonalId { get; set; }

        public bool Active { get; set; }

        public string Comment { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedLast { get; set; }

        public string OccupationName { get; set; }

        public string PhoneNumber { get; set; }

        public Int16? WorkHours { get; set; }

        public int? GroupId { get; set; }

        public byte[] Image { get; set; }

        public DateTime? Birthday { get; set; }

        public string Birthplace { get; set; }

        public string FamilyState { get; set; }

        public string Citizenship { get; set; }

        public string Residence { get; set; }

        public string Nation { get; set; }

        public string ContractNum { get; set; }

        public DateTime? ContractStartDate { get; set; }

        public DateTime? ContractEndDate { get; set; }

        public DateTime? PermitOfWork { get; set; }

        public bool? PermissionCallGuests { get; set; }

        public bool? MillitaryAssignment { get; set; }

        public String PersonalCode { get; set; }

        public String ExternalPersonalCode { get; set; }

        public int? LanguageId { get; set; }

        public DateTime RegistredStartDate { get; set; }

        public DateTime RegistredEndDate { get; set; }

        public int? TableNumber { get; set; }

        public virtual bool? WorkTime { get; set; }

        public bool? EServiceAllowed { get; set; }

        public bool? IsVisitor { get; set; }

        public bool? CardAlarm { get; set; }

        public string CreatedBy { get; set; }

        public bool IsDeleted { get; set; }
        [Timestamp]
        public byte[] Timestamp { get; set; }

        public int? CompanyId { get; set; }
        public int? TitleId { get; set; }
        public bool? IsShortTermVisitor { get; set; }

        public bool? ApproveTerminals { get; set; }
        public bool? ApproveVisitor { get; set; }

    }

    [Table("CameraIP")]
    public class CameraIP
    {
        [Key]
        public int CameraId { get; set; }
        public int? ServerId { get; set; }
        public string IP { get; set; }
        public string UID { get; set; }
        public string PWD { get; set; }
    }
    public class FSINISettings
    {
        [Key]
        public int Id { get; set; }
        public int SoftType { get; set; }
        public string Value { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class FSBuildingObjectCameras
    {
        [Key]
        public int Id { get; set; }
        public int BuildingObjectId { get; set; }
        public int CameraId { get; set; }
        public bool IsDeleted { get; set; }

    }

    public class FSVideoServers
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string IP { get; set; }
        public string UID { get; set; }
        public string PWD { get; set; }
    }

    public class FSBOC
    {
        [Key]
        public int Id { get; set; }
        public int BuildingObjectId { get; set; }
        public int CameraId { get; set; }
        public bool IsDeleted { get; set; }

    }


    [Table("CamPermission")]
    public class CamPermission
    {

        [Key]
        public int? CameraID { get; set; }
        public string CameraName { get; set; }

        public int? CompanyID { get; set; }

    }
    public class FSCameras
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string status { get; set; }

        public int ServerNr { get; set; }
        public int CameraNr { get; set; }
        public int Port { get; set; }
        public int ResX { get; set; }
        public int ResY { get; set; }
        public int Skip { get; set; }
        public int Delay { get; set; }
        public int QuickPreviewSeconds { get; set; }

        public bool Deleted { get; set; }
    }

    public class FSCamera_test
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ServerNr { get; set; }
        public int? CameraNr { get; set; }
        public int? Port { get; set; }
        public int? ResX { get; set; }
        public int? ResY { get; set; }
        public int? Skip { get; set; }
        public int? Delay { get; set; }
        public int? QuickPreviewSeconds { get; set; }

    }

    public class UserDepartments
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int DepartmentId { get; set; }
        public bool IsDepartmentManager { get; set; }

    }
    public class Hr_Clone
    {
        [Key]
        public int Id { get; set; }
        public string ois_id_isik { get; set; }
        public string Personal_code { get; set; }
        public string f_name { get; set; }
        public string l_name { get; set; }
        public string username { get; set; }
        public string dateform { get; set; }
        public string dateto { get; set; }
        public string email { get; set; }
        public string Address { get; set; }
    }
    public class Titles
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class TAReports
    {
        public int UserId { get; set; }
        public int DepartmentId { get; set; }
        public int BuildingId { get; set; }
    }
    public class Roles
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
    public class BuildingObjects
    {
        public int Id { get; set; }
        public int BuildingId { get; set; }
    }
    public class TAMoves
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int DepartmentId { get; set; }
        public string Label { get; set; }
        public string Remark { get; set; }
        public DateTime Started { get; set; }
        public DateTime Finished { get; set; }
        public float Hours { get; set; }
        public string Hours_Min { get; set; }
        public int Schedule { get; set; }
        public Nullable<byte> Status { get; set; }
        public bool JobNotMove { get; set; }
        public bool Completed { get; set; }

        public System.DateTime ModifiedLast { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.Int32> StartedBoId { get; set; }
        public Nullable<System.Int32> FinishedBoId { get; set; }

        public virtual User User { get; set; }
        public virtual Department Department { get; set; }
        public string UserName
        {
            get { return User.FirstName + " " + User.LastName; }
            set { }
        }
        public bool IsDeleted { get; set; }
        public int BuildingID { get; set; }

    }
    public class Box111
    {
        public int id { get; set; }
    }
    public class UserLog
    {
        [Key]
        public int id { get; set; }
        public int UserId { get; set; }
        public string UserFullName { get; set; }
        public string UserRole { get; set; }
        public string SearchBOx { get; set; }
        public DateTime Datefrom { get; set; }
        public DateTime dateto { get; set; }
        public string buildingfilter { get; set; }
        public string ShowDefaultOrfullLog { get; set; }
        public string Node { get; set; }
        public string CompanyFiter { get; set; }
        public string Name { get; set; }
        public string Filter_text { get; set; }
        public string user_text { get; set; }
        public string Activity { get; set; }
        public string button_clicked { get; set; }
        public int totalRecordView { get; set; }
        public DateTime VisitDate { get; set; }
    }

    public class FSHR
    {
        [Key]
        public int Id { get; set; }
        public string FoxSecFieldName { get; set; }
        public string HRFieldname { get; set; }
        public int FoxSecTableId { get; set; }
        public bool IsIndex { get; set; }
        public bool AutoUpdate { get; set; }
        public int FieldType { get; set; }
        public bool IsDeleted { get; set; }
        public string IndexFilename { get; set; }
        public string FoxsecTableName { get; set; }

        public List<FSHR> fsHrList { get; set; }
    }
    public class Buildingclass
    {
        public string Building { get; set; }
    }

    public class Countries
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int ISONumber { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class Locations
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class Terminal
    {
        [Key]
        public int Id { get; set; }
        public bool ShowScreensaver { get; set; }
        public TimeSpan ScreenSaverShowAfter { get; set; }
        public int MaxUserId { get; set; }
        public string CompanyId { get; set; }
        public string TerminalId { get; set; }
        public bool ApprovedDevice { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<bool> InfoKioskMode { get; set; }
        public string SoundAlarms { get; set; }
        public Nullable<int> ShowMaxAlarmsFistPage { get; set; }
        public Nullable<DateTime> LastLogin { get; set; }
    }

    //visitor
    public class Visitors
    {
        [Key]
        public int Id { get; set; }
        public virtual string CarNr { get; set; }

        /* [Key, ForeignKey("User")] */
        public virtual Nullable<int> UserId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string CarType { get; set; }
        public virtual Nullable<System.DateTime> StartDateTime { get; set; }
        public virtual Nullable<System.DateTime> StopDateTime { get; set; }
        public virtual bool IsDeleted { get; set; }
        public bool IsUpdated { get; set; }
        //public virtual Nullable<System.DateTime> UpdateDatetime { get; set; }
        public virtual DateTime? UpdateDatetime { get; set; }
        // public virtual System.DateTime LastChange { get; set; }
        public virtual DateTime? LastChange { get; set; }
        public virtual Nullable<int> CompanyId { get; set; }
        public virtual byte[] Timestamp { get; set; }
        public virtual bool Accept { get; set; }
        public virtual Nullable<int> AcceptUserId { get; set; }
        // public virtual Nullable<System.DateTime> AcceptDateTime { get; set; }
        // public virtual DateTime? AcceptDateTime { get; set; }
        public virtual DateTime? AcceptDateTime { get; set; }
        public virtual string LastName { get; set; }
        public virtual bool Active { get; set; }
        public virtual string Company { get; set; }
        public virtual Nullable<int> ParentVisitorsId { get; set; }
        public virtual string Comment { get; set; }
        // public virtual Nullable<System.DateTime> ReturnDate { get; set; }
        public virtual DateTime? ReturnDate { get; set; }
        public virtual string Email { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual bool IsCarNrAccessUnit { get; set; }
        public virtual bool IsPhoneNrAccessUnit { get; set; }
        public virtual Nullable<int> ResponsibleUserId { get; set; }
        public virtual bool CardNeedReturn { get; set; }
    }

    public class FoxSecDBContext : DbContext
    {
        public FoxSecDBContext() : base("FoxSecDBContext")
        { }

        public DbSet<Companies> Companies { get; set; }
        public DbSet<Departments> Departments { get; set; }

        public DbSet<Users> User { get; set; }

        public DbSet<Visitors> Visitor { get; set; }//Added

        public DbSet<UserPermissionGroup> UserPermissionGroups { get; set; } //Added
        public DbSet<UsersAccessUnit> UserAccessUnits { get; set; }
        public DbSet<FSINISettings> FSINISettings { get; set; }
        //public DbSet<CameraIP> CameraIPs { get; set; }
        public DbSet<FSBuildingObjectCameras> FSBuildingObjectCameras { get; set; }
        public DbSet<CamPermission> CamPermissions { get; set; }
        public DbSet<Buildings> Buildings { get; set; }
        public DbSet<TABuildingNames> TABuildingName { get; set; }
        public DbSet<FSCameras> FSCameras { get; set; }
        public DbSet<UserDepartments> UserDeparts { get; set; }
        public DbSet<Custormer> Custmers { get; set; }
        public DbSet<Hr_Clone> HrClones { get; set; }
        public DbSet<TAReport> TAreport { get; set; }
        //public DbSet<TAReports> TANewReoports { get; set; }
        public DbSet<Titles> Title { get; set; }

        // public DbSet<TAMove>
        public DbSet<TAMoves> NewTaMoves { get; set; }
        public DbSet<Roles> UserRoles { get; set; }


        public DbSet<BuildingObjects> BuildingObject { get; set; }

        public DbSet<UserLog> UserLogs { get; set; }
        public DbSet<Log> newlog { get; set; }
        public DbSet<FSHR> FSHR { get; set; }
        public DbSet<Countries> Countries { get; set; }
        public DbSet<Locations> Locations { get; set; }
        public DbSet<FSVideoServers> FSVideoServers { get; set; }
        public DbSet<Terminal> _Terminal { get; set; }
    }

}