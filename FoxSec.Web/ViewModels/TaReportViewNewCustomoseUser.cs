﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoxSec.Web.ViewModels
{
    public class TaReportViewNewCustomoseUser
    {
      
        public List<TaNewUserDetails> TaUserDetails { get; set; }
    }
    public class TaNewUserDetails
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalCode { get; set; }
        public string Name { get; set; }
        public DateTime Started { get; set; }
        public DateTime Finished { get; set; }
        public string checkin { get; set; }
        public double Hours { get; set; }
        public string checkout { get; set; }
        public string totaltime { get; set; }
        public int id { get; set; }
        public string FullName { get; set; }
        public int CompnyId { get; set; }
        public int buildngId { get; set; }
        public string companydeatils { get; set; }
        public string CompanyName { get; set; }
        public string CompanyRegistration_Num { get; set; }
        public string colorflag { get; set; }
        public string companycolrflag { get; set; }
        public string Department { get; set; }
    }
}