﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using FoxSec.Authentication;
using FoxSec.Common.SendMail;
using FoxSec.Core.Infrastructure.Bootstrapper;
using FoxSec.Core.Infrastructure.IoC;
using FoxSec.Core.Infrastructure.Localization;
using FoxSec.DomainModel.DomainObjects;
using FoxSec.Infrastructure.EF.Repositories;
using FoxSec.Infrastructure.EntLib.Logging;
using FoxSec.Web.Helpers;
using System.Web.Mvc;
using System.Data.Entity;
using FoxSec.Web.ViewModels;

namespace FoxSec.Web
{
    public class MvcApplication : HttpApplication
    {
        protected static void OnStart()
        {
            Bootstrapper.Run();
        }

        protected static void OnEnd()
        {
            IoC.Reset();
        }

        protected static void SetUILanguage()
        {
            var curr_lang = IoC.Resolve<ICurrentLanguage>();
            string cl = curr_lang.Get();

            if (string.IsNullOrWhiteSpace(cl))
            {
                cl = Literals.CULTURE_EN;
                curr_lang.Set(cl);
            }

            CultureInfo ci;

            try
            {
                ci = new CultureInfo(cl);
            }
            catch (ArgumentException)
            {
                curr_lang.Reset();
                curr_lang.Set(Literals.CULTURE_EN);
                ci = new CultureInfo(Literals.CULTURE_EN);
            }

            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
        }

        protected void OnError()
        {
            const string FORMAT = "{0}\nStack Trace:\n{1}";
            Exception exception = Server.GetLastError();
            var message = new StringBuilder();

            message.AppendFormat(FORMAT, exception.Message, exception.StackTrace);

            for (; exception.InnerException != null;)
            {
                message.Append("\nInner Exception:\n");
                message.AppendFormat(FORMAT, exception.InnerException.Message, exception.InnerException.StackTrace);
                exception = exception.InnerException;
            }

            IoC.Resolve<ILogger>().Write(message.ToString(), FoxSec.Infrastructure.EntLib.Literals.UNHANDLED_EXCEPTION_CATEGORY, TraceEventType.Critical);
        }

        protected void OnPostAuthenticateRequest()
        {
            IIdentity identity = Thread.CurrentPrincipal.Identity;

            if (!identity.IsAuthenticated)
            {
                return;
            }

            User user = IoC.Resolve<IUserRepository>().FindByLoginName(identity.Name);

            var activeUserRoles = user.UserRoles.Where(x => !x.IsDeleted && x.ValidFrom < DateTime.Now && x.ValidTo.AddDays(1) > DateTime.Now).ToList();

            int currentRoleId = 0;
            int? currentRoleTypeId = null;
            string currentRoleName = String.Empty;

            if (activeUserRoles.Count != 0)
            {
                currentRoleId = activeUserRoles.First().RoleId;
                currentRoleTypeId = activeUserRoles.First().Role.RoleTypeId;
                currentRoleName = activeUserRoles.First().Role.Name;//IoC.Resolve<IRoleRepository>().FindById(currentRoleId).Name;
            }

            IFoxSecIdentity fox_identity = new FoxSecIdentity(user.Id,
                                                                user.LoginName,
                                                                user.FirstName,
                                                                user.LastName,
                                                                user.Email,

                                                                identity.AuthenticationType,

                                                                user.GetPermissions() == null ? null : user.GetPermissions().AsReadOnly(),
                                                                user.GetMenues() == null ? null : user.GetMenues().AsReadOnly(),
                                                                currentRoleId,
                                                                currentRoleTypeId,
                                                                currentRoleName,

                                                                user.CompanyId,


                                                                Request.UserHostAddress

                                                                );

            Thread.CurrentPrincipal = HttpContext.Current.User = new FoxSecPrincipal(fox_identity);
        }

        protected void Application_Start()
        {
            OnStart();
            //illi 25.12.1012   Logger4SendingEMail.InitLogger();
            //IoC.Resolve<ICurrentLanguage>().Set("EN");
            ModelBinders.Binders.DefaultBinder = new DevExpress.Web.Mvc.DevExpressEditorsBinder();
            //Database.SetInitializer<FoxSecDBContext>(new DropCreateDatabaseIfModelChanges<FoxSecDBContext>());
            Database.SetInitializer<FoxSecDBContext>(null);
        }

        protected void Application_End()
        {
            OnEnd();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            SetUILanguage();
        }

        protected void Application_Error()
        {
            OnError();
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            OnPostAuthenticateRequest();
        }
        protected void Session_Start(Object sender, EventArgs e)
        { //illi natuke kahtlane security ,võimalus muukida
            Session["init"] = 0;
        }
    }
}